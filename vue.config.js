const path = require("path");

module.exports = {
  runtimeCompiler: true,
  css: {
    requireModuleExtension: true
  },

  pluginOptions: {
    "style-resources-loader": {
      preProcessor: "scss",
      patterns: [
        path.resolve(__dirname, "./src/assets/scss/functions.scss"),
        path.resolve(__dirname, "./src/assets/scss/mixins.scss"),
        path.resolve(__dirname, "./src/assets/scss/variables.scss")
      ]
    }
  }
};
