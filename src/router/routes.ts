import { list } from "@/config/routes";
import { route, RouteType } from "@/router/utils";

const routes = [...list.map((item: RouteType) => route(item))];

export { routes };
