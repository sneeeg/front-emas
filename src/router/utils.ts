import store from "@/store";
import { NavigationGuardNext, Route } from "vue-router";
import { UserInterface, UserRole } from "@/helpers/types";

export const ifNotAuthenticated = (
  to: Route,
  from: Route,
  next: NavigationGuardNext
) => {
  if (!store.getters.isAuthenticated) {
    next();
    return;
  }
  next("/courses/list");
};

export const ifAuthenticated = (
  to: Route,
  from: Route,
  next: NavigationGuardNext
) => {
  if (store.getters.isAuthenticated) {
    next();
    return;
  }
  next("/auth");
};

export type RouteType = {
  name: string;
  path: string;
  menuIcon?: string;
  component: string;
  roleComponent?: Partial<Record<UserRole, string>>;
  layout?: string;
  meta?: {
    navBack?: string;
    roles: string[];
    title: string;
    roleTitle?: Partial<Record<UserRole, string>>;
    layout: string;
  };
  redirect?: string;
  private?: boolean;
  disabled?: boolean;
};

export const route = (route: RouteType) => ({
  ...route,
  beforeEnter: (to: Route, from: Route, next: NavigationGuardNext) => {
    if (route.private) {
      ifAuthenticated(to, from, next);
    } else {
      ifNotAuthenticated(to, from, next);
    }
  },
  component: (page: any) => {
    const profile = store.getters.getProfile;
    const role = profile?.roles?.[0];
    const component = route?.roleComponent?.[role as UserRole];

    if (role && component) {
      return import(`@/pages/${component}.vue`).then(page);
    }

    return import(`@/pages/${route.component}.vue`).then(page);
  }
});
