import Vue from "vue";
import Router from "vue-router";
import store from "@/store";
import { routes } from "./routes";

import { setPageTitle } from "@/helpers/utils/functions";

Vue.use(Router);

const router = new Router({
  mode: "history",
  routes
});

router.beforeEach(async (to: any, from: any, next) => {
  const { isAuthenticated } = store.getters;

  if (isAuthenticated) {
    if (!store.getters.getProfile) {
      try {
        await store.dispatch("fetchProfile");
      } catch (e) {
        console.log(e);
      }
    }
    const profile = store.getters.getProfile;

    if (profile) {
      const { roles } = profile;

      const allowedRoles: string[] = to.matched.reduce((acc: any, route: any) => {
        return route.meta.roles && !acc.length ? route.meta.roles : acc;
      }, []);

      const hasAccess = roles.some((role: string) =>
        allowedRoles.includes(role)
      );

      if (hasAccess) {
        setPageTitle(to.meta.title);
        next();
      } else {
        const redirectRouteByRole: Record<string, { name: string }> = {
          admin: { name: "courses.admin" },
          superadmin: { name: "courses.admin" },
          moderator: { name: "users.index" },
          trainer: { name: "project.index" },
          student: { name: "profile.index" },
        };
        next(redirectRouteByRole[roles[0]]);
      }
    }
  } else {
    next();
  }
});

export default router;
