export const REPORT_URL = {
  // Результаты
  QUIZ_SCORE_RESULTS: "/admin/report/quizScoreResults",

  // Анализ ответов
  QUIZ_ANSWER_BREAKDOWN: "/admin/report/quizAnswerBreakdown",

  // Детали попыток
  QUIZ_ATTEMPT_DETAIL: "",

  // Достижения учащихся
  PROGRESS_ACHIEVEMENT: "",

  // Результаты заданий
  ASSIGNMENT_RESULTS: "",

  // Действия
  CONTENT_ACTIVITY: "",

  // Трафик
  PRESENTATION_TRAFFIC: "",

  // Прогресс
  PRESENTATION_PROGRESS: "",

  // Популярный материал
  PRESENTATION_POPULAR: "",

  // Просмотры слайда или страницы
  PRESENTATION_SLIDE_VIEWS: "",

  // Сводка достижений
  PERFORMANCE_SUMMARY: "",

  // Сводка по программам обучения
  LEARNING_PATH_SUMMARY: "/admin/report/learningPathSummary",

  // Сводка по просмотренным новостям
  LOOKED_NEWS: "/admin/report/lookedNews",

  // Сводка по просмотренным новостям
  SUBMITTED_PROJECT: "/admin/report/submittedProject",

  // Детали по программе обучения
  LEARNING_PATH_DETAILS: "",

  // Действия группы
  GROUP_ACTIVITY: "",

  // Действия пользователя
  USER_ACTIVITY: "/admin/report/getUserActivity",

  // Действия пользователя
  TRAFFIC_LIGHTS: "/admin/report/trafficLights",

  // Активные группы
  ACTIVE_GROUPS: "",

  // Активные пользователи
  ACTIVE_USERS: "",

  // Гостевая книга
  GUESTBOOK_RESPONSES: "",

  // История продаж
  SALES_HISTORY: "",

  // Сводка продаж
  SALES_SUMMARY: "",

  // Посещение мероприятий
  EVENTS_ATTENDANCE: "",

  // Проведенные мероприятия
  ACCOMPLISHED_EVENTS: ""
};
