const URL = {
  // Главная
  INDEX: "/",

  // Страница авторизации
  AUTH: "/auth",

  // Страница авторизации
  ADMIN: "/admin",

  // Страница пользователя
  PROFILE: "/profile",

  // Страница курсов
  COURSES: "/courses",
  // Страница курсов
  CONTENT: "/content",

  // Страница курсов
  COURSES_DONE: "/courses-done",

  // Страница файлового менеджера
  FILE_MANAGER: "/file-manager",

  // Страница файлов
  FILE: "/file",

  // Страница группы
  GROUP: "/group",

  // Страница пользователей
  USERS: "/users",

  // Страница проектов студентов
  PROJECTS: "/projects",

  // Страница настроек системы
  SETTING: "/setting",

  // Страница рейтинга
  RATING: "/rating",

  // Страница шаблонов
  TEMPLATES: "/templates",

  // Страница документов у юзера
  DOCUMENTS: "/documents",

  // Страница цен
  PRICE: "/price",

  // Страница документов у юзера
  UPLOAD: "/upload",

  // Страница отчётов
  REPORTS: "/reports",

  // Страница тестов
  QUIZZES: "/quizzes",

  // Страница видео
  VIDEOS: "/videos",

  // Страница отправки писем
  MAIL: "/mail",

  // Страница оценок
  SCORE: "/score",

  // Страница нетворкинга
  NETWORKING: "/networking",

  // Страница оплаты
  PAYMENT: "/payment",

  // Страница новостей
  NEWS: "/news",

  // Страница 404
  PAGE_NOT_FOUND: "*"
};

export { URL };
