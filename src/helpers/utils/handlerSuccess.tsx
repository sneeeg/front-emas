import Vue from "vue";

interface HandlerSuccessInterface {
  title: string;
  text: string;
}

const handlerSuccess = (message: HandlerSuccessInterface) => {
  const { title, text } = message;

  Vue.notify({
    type: "success",
    title,
    text
  });
};

export { handlerSuccess };
