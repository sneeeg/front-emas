import Vue from "vue";

interface HandlerErrorInterface {
  data: any;
  errorCategory: string;
  errorName: string;
  errorVars: (string | number)[];
}

const handlerErrorPass = (error: HandlerErrorInterface) => {
  const { errorCategory, errorName, data } = error;

  Vue.notify({
    type: "error",
    title: errorName,
    text: data.confirm_password[0]
  });

  //throw new Error(errorName);
};

export { handlerErrorPass };
