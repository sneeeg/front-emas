import axios from "axios";
import { handlerErrorDownload } from "@/helpers/utils/handlerErrorDownload";

const downloader = async (
  id: number,
  path = (id: number) => `/api/document/${id}/download`,
  filename?: string,
  open?: boolean
) => {
  await axios
    .get(path(id), {
      ...(filename && {
        responseType: "arraybuffer"
      })
    })
    .then(async response => {
      console.log(response)
      if (filename && open) {
        const blob = new Blob([response.data], {
          type: response.headers["content-type"]
        });
        const blobURL = URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = blobURL;
        document.body.appendChild(link);
        link.click();
        link.remove();
      } else if (filename) {
        const blob = new Blob([response.data], {
          type: response.headers["content-type"]
        });
        const blobURL = URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = blobURL;
        let fileName;
        if (filename) {
          fileName = filename.substr(filename.lastIndexOf("/") + 1);
        } else if (response.headers["X-Suggested-Filename"]) {
          fileName = response.headers["X-Suggested-Filename"];
        } else if (response.headers["content-disposition"]) {
          // eslint-disable-next-line prefer-destructuring
          fileName = response.headers["content-disposition"].split(
            "filename="
          )[1];
        } else if (response.data?.data?.file_name) {
          fileName = response.data?.data?.file_name;
        } else {
          fileName = "file";
        }
        fileName = fileName.replace(/"/g, "");
        link.setAttribute("download", fileName);
        document.body.appendChild(link);
        link.click();
        link.remove();
      } else if (response.data.data.url) {
        window.open(response.data.data.url, "_blank");
      }
    })
    .catch(error => {});
};

export { downloader };
