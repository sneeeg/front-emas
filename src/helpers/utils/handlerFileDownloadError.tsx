import Vue from "vue";
import i18n from "@/i18n"
interface HandlerErrorInterface {
  errorCategory: string;
  errorName: string;
  errorVars: (string)[];
}

const handlerFileDownloadError = (error: HandlerErrorInterface) => {
  const { errorCategory, errorName, errorVars } = error;
  const errorVarsTwo: (string)[] = [];
  errorVars.forEach(element => {
    errorVarsTwo.push(String(i18n.t(element)))
  });
  if (errorVars) {
    Vue.notify({
      type: "error",
      title: String(i18n.t('error_download')),
      text: String(i18n.t(errorName)) + errorVarsTwo.join(', ')
    });
  } else {
    Vue.notify({
      type: "error",
      title: String(i18n.t('error_download')),
      text: String(i18n.t(errorName))
    });
  }
};

export { handlerFileDownloadError };
