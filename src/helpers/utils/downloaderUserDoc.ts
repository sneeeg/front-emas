import axios from "axios";
import { handlerError } from "@/helpers/utils/handlerError";
const mime = require("mime-types");
const contentDisposition = require("content-disposition");
const downloadFile = (response: any) => {
  let fileName;
  let fileExt;
  console.log("response", response);
  if (response.headers["X-Suggested-Filename"]) {
    fileName = response.header["X-Suggested-Filename"];
  } else if (response.headers["content-disposition"]) {
    fileName = contentDisposition.parse(response.headers["content-disposition"])
      .parameters.filename;
  } else if (response.data?.data?.file_name) {
    fileName = response.data?.data?.file_name;
  } else {
    fileName = "file";
  }

  //if (response.headers["content-type"]) {
  //  fileExt = mime.extension(response.headers["content-type"]);
  //}
  fileName = fileName.replace(/"/g, "");
  const url = window.URL.createObjectURL(new Blob([response.data]));
  const link = document.createElement("a");
  link.href = url;
  link.setAttribute("download", fileName);
  document.body.appendChild(link);
  link.click();
  link.remove();
};

const downloaderUserDoc = async (id: number, path: string) => {
  await axios
    .get(path, { responseType: "blob" })
    .then(response => {
      downloadFile(response);
    })
    .catch(error => {
      if (error.response.status === 400) {
        handlerError(error.response.data);
      }
    });
};

export { downloaderUserDoc, downloadFile };
