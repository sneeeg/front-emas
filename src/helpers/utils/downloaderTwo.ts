import axios from "axios";
import { handlerErrorDownload } from "@/helpers/utils/handlerErrorDownload";
const downloadFile = (response: any) => {
  let fileName: any;
  console.log("response", response);
  if (response.headers["X-Suggested-Filename"]) {
    fileName = response.header["X-Suggested-Filename"];
  } else if (response.headers["content-disposition"]) {
    // eslint-disable-next-line prefer-destructuring
    fileName = response.headers["content-disposition"].split("filename=")[1];
  } else if (response.data?.data?.file_name) {
    fileName = response.data?.data?.file_name;
  } else {
    fileName = "file";
  }
  fileName = fileName.replace(/"/g, "");
  axios({
    url: response.data?.data?.url,
    method: 'GET',
    responseType: 'blob', // important
  }).then((response) => {
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', fileName);
    document.body.appendChild(link);
    link.click();
  });
  
};

const downloaderTwo = async (
  id: number,
  path = (id: number) => `/api/document/${id}/download`
) => {
  await axios
    .get(path(id))
    .then(response => {
      //console.log(response);
      downloadFile(response);
      //this.finishLoading();
    }).catch(error => {
      handlerErrorDownload(error)
      //console.log(error)
    });
};

export { downloaderTwo, downloadFile };
