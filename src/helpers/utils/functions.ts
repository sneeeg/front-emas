import { document_title } from "@/config";
import i18n from "@/i18n";
const moment = require("moment");
export const makeFormattedMediaType = (type: string): string => {
  const types: { [key: string]: string } = {
    "text/plain": "document",
    "video/mp4": "video",
    "video/m3u8": "video",
    default: "file"
  };
  return types[type] ? types[type] : types["default"];
};

export const scrollToElement = (id: string, timeout = 0) => {
  const el = document.getElementById(id);
  if (el) {
    setTimeout(() => {
      el.scrollIntoView({ behavior: "smooth", block: "end" });
    }, timeout);
  }
};

export const setPageTitle = (title?: string) => {
  const defaultTitle = document_title;
  document.title = defaultTitle;
};

export const makeQueryURL = (data: any) => {
  return new URLSearchParams(data).toString();
};

export const numberWithCommas = (x = 0, commas = " ") => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, commas);
};
/** YYYY-MM-DD */
export const DateToString = (date: any) => {
  if (date === null) {
    return null;
  }
  if (date === "Invalid date") {
    return null;
  }
  if (moment(date, "DD.MM.YYYY", true).isValid()) {
    let Date = date;
    Date = moment(Date, "DD.MM.YYYY").toString();
    Date = moment(Date).format("YYYY-MM-DD");
    return Date;
  } else {
    return date;
  }
};
/** DD.MM.YYYY */
export const StringToDate = (date: any) => {
  if (date === null) {
    return null;
  }
  if (date === "Invalid date") {
    return null;
  }
  if (moment(date, "YYYY-MM-DD", true).isValid()) {
    let Date = date;
    Date = moment(Date, "YYYY-MM-DD").toString();
    Date = moment(Date).format("DD.MM.YYYY");
    return Date;
  } else {
    return date;
  }
};

export const isEmptyObject = (obj: Record<string, any>): boolean => {
  if (!obj) {
    return true;
  } else {
    const checkValues = obj && Object.values(obj).filter(v => v);
    return Object.keys(obj).length === 0 || checkValues.length === 0;
  }
};

export const isEmptyArray = (array: unknown[]) => {
  if (typeof array !== "undefined" && array !== null) {
    return array.length === 0;
  } else {
    return true;
  }
};

export const makeFormData = (object: any) => {
  const formData = new FormData();
  Object.keys(object).forEach(key => formData.append(key, object[key]));
  return formData;
};

export const arrayToObject = (array: any[]): Record<string, any> => {
  return array.reduce((obj, item) => {
    const key = Object.entries(item)[0][0];
    const value = Object.entries(item)[0][1];

    if (key) obj[key] = value;

    return obj;
  }, {});
};

export const formatDateToString = (date: Date | null) => {
  if (!date) return null;

  const day = date.getDate();
  const month = date.getMonth();

  // 01, 02, 03, ... 10, 11, 12
  const dd = (day < 10 ? "0" : "") + day;
  // 1970, 1971, ... 2020, 2021, ...
  const MM = (month + 1 < 10 ? "0" : "") + (month + 1);
  const yyyy = date.getFullYear();

  return `${yyyy}-${MM}-${dd}`;
};

export const getFieldInObject = <T, K extends keyof T>(object: T, field: K) => {
  return {
    ...(field in object && {
      [field]: object?.[field]
    })
  };
};

export const toSnakeCase = (string: string, upper = false): string => {
  return (
    string &&
    string
      .match(
        /[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g
      )!
      .map(x => (upper ? x.toUpperCase() : x.toLowerCase()))
      .join("_")
  );
};

export const numberToTime = (secNum: number, showHour = true) => {
  let hours: number | string = Math.floor(secNum / 3600);
  let minutes: number | string = Math.floor((secNum - hours * 3600) / 60);
  let seconds: number | string = secNum - hours * 3600 - minutes * 60;

  if (hours < 10) {
    hours = `0${hours}`;
  }
  if (minutes < 10) {
    minutes = `0${minutes}`;
  }
  if (seconds < 10) {
    seconds = `0${seconds}`;
  }

  if (showHour) {
    return `${hours}:${minutes}:${seconds}`;
  } else {
    return `${minutes}:${seconds}`;
  }
};
