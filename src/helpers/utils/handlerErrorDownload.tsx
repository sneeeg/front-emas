import Vue from "vue";
import i18n from "@/i18n"

interface HandlerErrorInterface {
  errorCategory: string;
  errorName: string;
  errorVars: string[];
}

const handlerErrorDownload = (error: any) => {
  //console.log(error.response)
  Vue.notify({
    type: "error",
    title: String(i18n.t(error.response.statusText)),
    text: String(i18n.t(error.response.status))
  });
};

export { handlerErrorDownload };
