import axios from "axios";
import { makeFormData } from "@/helpers/utils/functions";
import { handlerError } from "@/helpers/utils/handlerError";

const uploader = async (
  path: string,
  params: any,
  callback: (percent: any) => any
) => {
  const fd = makeFormData(params);
  return await axios
    .post(path, fd, {
      headers: {
        "content-type": "multipart/form-data"
      },
      onUploadProgress: ({ loaded, total }) => {
        const percent = Math.round((loaded * 100) / total);
        console.log("percent", percent);

        callback(percent);
        return percent;
      }
    })
    .catch(error => {
      handlerError(error?.response?.data);
      throw Error(error);
    });
};

export { uploader };
