import Vue from "vue";
import i18n from "@/i18n"

interface HandlerErrorInterface {
  errorCategory: string;
  errorName: string;
  errorVars: string[];
}

const handlerError = (error: HandlerErrorInterface) => {
  const { errorCategory, errorName, errorVars } = error;
  const errorVarsTwo: string[] = [];
  if (!error.errorCategory && !error.errorName && !error.errorVars) {
    Vue.notify({
      type: "error",
      title: String(i18n.t('server_error')),
      text: String(i18n.t('server_error'))
    });
  }
  if (errorVars) {
    errorVars.forEach(element => {
      errorVarsTwo.push(String(i18n.t(element)))
    });
    Vue.notify({
      type: "error",
      title: String(i18n.t(errorCategory)),
      text: String(i18n.t(errorName)) + errorVarsTwo.join(', ')
    });
  } else {
    Vue.notify({
      type: "error",
      title: String(i18n.t(errorCategory)),
      text: String(i18n.t(errorName))
    });
  }
};

export { handlerError };
