import { Maybe } from "@/helpers/types/common";
import { TranslateResult } from "vue-i18n";

/**
 * Тип отчёта. Соответствует url-адресу отчёта
 */
export enum ReportTypes {
  QUIZ_SCORE_RESULTS = "quiz-score-results",
  QUIZ_ANSWER_BREAKDOWN = "quiz-answer-breakdown",
  // QUIZ_ATTEMPT_DETAIL = "quiz-attempt-detail",
  // PROGRESS_ACHIEVEMENT = "progress-achievement",
  // ASSIGNMENT_RESULTS = "assignment-results",
  // CONTENT_ACTIVITY = "content-activity",
  // PRESENTATION_TRAFFIC = "presentation-traffic",
  // PRESENTATION_PROGRESS = "presentation-progress",
  // PRESENTATION_POPULAR = "presentation-popular",
  // PRESENTATION_SLIDE_VIEWS = "presentation-slide-views",
  // PERFORMANCE_SUMMARY = "performance-summary",

  //PROGRESS_PROGRAM = "progressProgram",

  LEARNING_PATH_SUMMARY = "learning-path-summary",
  LOOKED_NEWS = "lookedNews",
  SUBMITTED_PROJECT = "submittedProject",
  // LEARNING_PATH_DETAILS = "learning-path-details",
  // GROUP_ACTIVITY = "group-activity",
  USER_ACTIVITY = "user-activity",
  TRAFFIC_LIGHTS = "traffic-lights"
  // ACTIVE_GROUPS = "active-groups",
  // ACTIVE_USERS = "active-users",
  // GUESTBOOK_RESPONSES = "guestbook-responses",
  // SALES_HISTORY = "sales-history",
  // SALES_SUMMARY = "sales-summary",
  // EVENTS_ATTENDANCE = "events-attendance",
  // ACCOMPLISHED_EVENTS = "accomplished-events"
}

/**
 * Тип карточки на странице отчёта
 */
export enum ReportView {
  TABLE = "table",
  PARAMS = "params",
  RESULT = "result",
  STEP = "step"
}

/**
 * Локализация быстрого выбора диапазона
 */
export enum ReportParamsDateRangeLocale {
  ALL_TIME = "За всё время",
  YEAR = "За год",
  QUARTER = "За квартал",
  MONTH = "За месяц",
  WEEK = "За неделю",
  YESTERDAY = "Вчера"
}

/**
 * Ссылка на страницу отчёта
 */
export type ReportLinkChildType = {
  id: ReportTypes;
  iconName: string;
  name: string;
  description: string;
  disabled?: true;
};

/**
 * Блок со ссылками на страницы отчётов
 */
export type ReportLinkType = {
  title: TranslateResult;
  children: ReportLinkChildType[];
};

/**
 * Тип мероприятия
 */
export type ReportEventTypeType = {
  id: "all" | "webinar" | "training";
  label: "Все мероприятия" | "Вебинар" | "Тренинг";
};

/**
 * Шаблон карточки с таблицей
 */
export type ReportCardTableType = {
  /** Название геттера, который получает данные. Например, `getUsers` */
  getter: string;
  /** Название экшена, который фетчит данные. Например, `fetchUsers` */
  action: string;
  /** Поле с названием элемента в таблице */
  titleField: string;
  /** Поле с названием элемента в таблице */
  selectType: "single" | "multiple";

  type?: string;
};

/**
 * Возвращаемые данные из карточки с таблицей
 */
export type ReportCardReturnedTableType = any[];

/**
 * Шаблон карточки с параметрами
 */
export type ReportCardParamsType = {
  eventType?: ReportEventTypeType[] | undefined;
  group?: string | undefined;
  datepicker?:
    | Maybe<Date | Date[] | number | number[] | string | string[]>
    | undefined;
  requiredDatepicker?: boolean;
  user?: string | undefined;
  step?: Array<any>;
  defaultStep?: { value: string; title: string };
  showAllUsers?: boolean | undefined;
  onlyUnverified?: boolean | undefined;
  hideUnassigned?: boolean | undefined;
  warningMessage?: string | undefined;
};

/**
 * Возвращаемые данные из карточки с параметрами
 */
export type ReportCardReturnedParamsType = {
  eventType?: { id: string; label: string } | string | undefined;
  group_id?: number | "";
  dateRange?:
    | Maybe<string | number | number[] | Date | Date[] | string[]>
    | undefined;
  user?: string | undefined;
  showAllUsers?: boolean | undefined;
  showArchived?: boolean | undefined;
  onlyUnverified?: boolean | undefined;
  hideUnassigned?: boolean | undefined;
  step?: string;
};

/**
 * Шаблон карточки с кнопкой "Создать отчёт"
 */
export type ReportCardResultType = {
  showReportBelow: boolean;
};

/**
 * Возвращаемые данные из карточки с кнопкой "Создать отчёт"
 */
export type ReportCardReturnedResultType = {
  showReportBelow: boolean;
};

/**
 * Шаблон карточки
 */
export type ReportCardType = {
  title?: string | undefined;
  view: ReportView;
  content?:
    | ReportCardTableType
    | ReportCardParamsType
    | ReportCardResultType
    | undefined;
};

/**
 * Шаблон страницы
 */
export type ReportLayoutType = {
  cards: ReportCardType[];
  result: {
    showInfo: boolean;
    type?: string;
    table: {
      columns: {
        field: string;
        label: string;
        width?: string;
        type?: "number" | "decimal" | "percentage" | "boolean" | "date";
        dateInputFormat?: any;
        dateOutputFormat?: any;
        sortable?: boolean;
        formatFn?: (value: any) => any;
      }[];
    };
  };
};

/**
 * Шаблоны страниц отчётов
 */
export type ReportLayoutsType = {
  [key in ReportTypes]: ReportLayoutType;
};

// ----------------------- Request
/**
 * Параметры за запроса на сервер
 */
export type ReportRequestParamsType = {
  quiz_id?: number | string | undefined;
  user_id?: number | string | undefined;
  program_id?: number[] | string | undefined;
  users?: any[];
  group_id?: number | "";
  date_start?: string | undefined;
  date_end?: string | undefined;
  hide_unassigned?: boolean | undefined;
  step?: string;
};

/**
 * Запрос на сервер
 */
export type ReportRequestType = {
  type: ReportTypes | "";
  params: ReportRequestParamsType;
};

/**
 * Ответ сервера при ошибке
 */
export type ReportResponseErrorType = {
  data: number;
  errorCategory: string;
  errorName: string;
  errorVars: any[];
  success: false;
};

/**
 * Ответ сервера при успехе
 */
export type ReportResponseSuccessType<Data> = {
  data: Data;
  success: true;
};

/**
 * Ответ сервера
 */
export type ReportResponseType<Data> =
  | ReportResponseSuccessType<Data>
  | ReportResponseErrorType;

/**
 * Объект, который вставляется в таблицу отчёта
 */
export type ReportType = {
  date_end?: Maybe<string>;
  date_plan?: Maybe<{
    date: Maybe<string>;
    timezone: Maybe<string>;
    timezone_type: Maybe<number>;
  }>;
  group_id?: number[];
  group_name?: Maybe<Maybe<string>[]>;
  name?: Maybe<string>;
  status?: Maybe<number>;
  user?: Maybe<string>;
};
