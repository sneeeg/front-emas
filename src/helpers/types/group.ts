interface GroupInterface {
  created_at: string;
  description: string;
  id: number;
  name: string;
  group?: boolean;
  pivot?: {
    program_id: string;
    group_id: string;
  };
  group_id: string;
  program_id: string;
  updated_at: string;
}

export { GroupInterface };
