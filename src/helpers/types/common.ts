type Maybe<T> = T | null;

type Exact<T extends { [key: string]: unknown }> = {
    [K in keyof T]: T[K];
};

type Scalars = {
    ID: string;
    String: string;
    Boolean: boolean;
    Int: number;
    Float: number;
    Date: any;
    Time: any;
};

interface DataInterface<Data> {
    [key: string]: Data | null | number | string | undefined;
    current_page: number;
    data: Data;
    first_page_url: string;
    from: number;
    last_page: number;
    last_page_url: string;
    next_page_url: string;
    path: string;
    per_page: number;
    prev_page_url?: Maybe<string>;
    to: number;
    total: number;
}

interface FolderInterface {
    name: string
    path: string
}

interface MediaInterface {
    collection_name?: string
    created_at?: string
    days?: number
    disk?: string
    id_media?: number
    type?: string
    duration?: number
    end_path?: string
    file_name?: string
    id?: number
    is_project?: boolean
    type_score?: string
    mime_type?: string
    model_id?: string
    model_type?: string
    name?: string
    name_front?: string
    order_column?: string;
    try_date_came_out: string;
    try_in_time: string;
    pivot?: {
        program_chapter_id: string,
        media_id: string
    },
    responsive_images?: number[]
    size?: string
    updated_at?: string
}

interface RequestInterface<Data> {
    data: Data
    message: Maybe<string>
    success: boolean
    errorCategory: Maybe<string>
    errorName: Maybe<string>
}

export {
    DataInterface,
    FolderInterface,
    MediaInterface,
    RequestInterface
}

// eslint-disable-next-line prettier/prettier
export type {
    Maybe,
    Exact,
    Scalars
}
