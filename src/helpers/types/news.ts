interface NewsInterface {
  id: number;
  title: string;
  body: string;
  lang: string;
  date_end: string;
  required: string;
  created_at?: string;
  updated_at?: string;
}

export { NewsInterface };
