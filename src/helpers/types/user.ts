import { GroupInterface } from "@/helpers/types/group";

enum UserRole {
  admin = "admin",
  superadmin = "superadmin",
  moderator = "moderator",
  trainer = "trainer",
  student = "student"
}

interface UserInterface {
  remember?: boolean;
  CP_bank?: string;
  CP_bik?: string;
  CP_ceo?: string;
  CP_ceofio?: string;
  CP_doc?: string;
  CP_inn?: string;
  CP_kpp?: string;
  CP_ks?: string;
  CP_lawaddress?: string;
  CP_name?: string;
  CP_ogrn?: string;
  CP_postaddress?: string;
  CP_rs?: string;
  age?: number;
  apartment?: string;
  avatar?: any;
  avatar_id?: string;
  birthday_date?: string;
  block?: boolean;
  citizenship?: string;
  city?: string;
  company?: string;
  companyPayment?: string;
  company_activity?: string;
  country?: string;
  created_at?: string;
  currency?: string;
  currencyDop?: string;
  d_date?: string;
  d_num?: string;
  date_block?: string;
  date_start?: string;
  date_termination?: string;
  displayname?: string;
  diploma?: string;
  diploma_ser?: string;
  duration?: number;
  email?: string;
  email_verified_at?: string;
  first_name?: string;
  format?: string;
  gender?: "Male" | "Female" | "no-gender";
  goal?: any;
  groups?: GroupInterface[];
  house?: string;
  housing?: string;
  id?: number;
  is_start?: number;
  lang?: string;
  last_name?: string;
  last_name_latin?: string;
  manage_exp?: string;
  name?: string;
  first_name_latin?: string;
  nameOrgan?: string;
  no_notifications?: boolean;
  isSelectProgram?: boolean;
  passport_code?: string;
  passport_number?: string;
  passport_number_when?: string;
  passport_number_who?: string;
  passport_series?: string;
  patronymic?: string;
  payment?: number;
  payment_parts?: any;
  payments?: any[];
  phone?: string;
  plan?: any[];
  position_company?: string;
  priceSelected?: number;
  priceSelectedDop?: number;
  profitability_business?: string;
  programGraph?: string;
  programView?: string;
  programs?: any[];
  session_count?: number;
  sessions?: any;
  session_days?: string;
  cusRoles?: string[];
  roles?: UserRole[];
  source?: any;
  street?: string;
  typeID?: string;
  updated_at?: string;
  work_exp?: string;
  year_diploma?: string;
  zip?: string;
  username?: string;
  password?: string;
  google_calendar?: string;
  google_calendar_api?: string;
  inst?: string;
  vk?: string;
  facebook?: string;
  linkedin?: string;
  twitter?: string;
  role_slug?: string;
}

export { UserRole, UserInterface };
