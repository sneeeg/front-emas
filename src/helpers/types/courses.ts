import { MediaInterface } from "@/helpers/types/common";
import { UserInterface } from "@/helpers/types/user";
import { GroupInterface } from "@/helpers/types/group";

interface CourseInterface {
  author_id?: string;
  chapter?: ChapterInterface[];
  created_at?: string;
  description?: string;
  thumb_url?: string;
  completion_order?: any;
  duration?: number;
  id?: number;
  name: string;
  session?: any;
  progress?: number;
  updated_at?: string;
  group?: GroupInterface[];
  users?: UserInterface[];
}

interface ChapterInterface {
  created_at?: string;
  days?: string;
  duration?: string;
  id?: number;
  media?: MediaInterface[];
  name: string;
  program_id: number;
  delete?: boolean;
  edit?: boolean;
  quizzes?: any;
  updated_at?: string;
  sort?: number;
}

export { CourseInterface, ChapterInterface };
