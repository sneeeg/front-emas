export { UserRole, UserInterface } from "./user";
export { NewsInterface } from "./news";
export { GroupInterface } from "./group";
export { DataInterface, FolderInterface, MediaInterface } from "./common";
export { CourseInterface, ChapterInterface } from "./courses";
export { ReportTypes, ReportView, ReportParamsDateRangeLocale } from "./reports"

// eslint-disable-next-line prettier/prettier
export type {
    ReportLinkChildType,
    ReportLinkType,
    ReportCardTableType,
    ReportCardReturnedTableType,
    ReportCardParamsType,
    ReportCardReturnedParamsType,
    ReportCardResultType,
    ReportCardReturnedResultType,
    ReportCardType,
    ReportLayoutType,
    ReportLayoutsType,
    ReportRequestType,
    ReportResponseType,
    ReportRequestParamsType,
    ReportType
} from "./reports";

export type {
    Maybe,
    Exact,
    Scalars
} from './common';
