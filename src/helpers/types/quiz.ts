import { Maybe } from "@/helpers/types/common";

export interface QuestionUserAnswer {
  /** ID теста */
  quiz_id: Quiz["id"];

  /** ID вопроса */
  question_id: Question["id"];

  /**
   * @description ID ответа
   * @description Можно отправлять один или несколько ответов на вопрос сразу.
   *  Даже если ответ один, он должен быть массивом.
   *  Если вопрос имеет значение `radio` и будет отправлено несколько ответов, то придёт ошибка.
   * @example
   *  answer_id: [4, 8, 15, 16, 23, 42]
   *  // or
   *  answer_id: [42]
   */
  answer_id: number[];

  /**
   * @description Альтернативный ответ.
   * @description Должно быть только `answer_id` или только `alternative`.
   *  Если будет задан хоть какой-то `answer_id`, даже пустой, то значение `alternative` будет игнорироваться.
   */
  alternative?: string;
}

export interface QuestionOption {
  /** ID варианта ответа */
  id?: number;

  /** Вариант ответа */
  option: string;

  /** К какому вопросу относится этот вариант ответа */
  question_id: number;

  /** Кол-во баллов за этот ответ. Может быть в минусе */
  score: number;

  created_at_front?: string;

  image?: File;
}

export interface Question {
  /** ID вопроса */
  id?: string;

  /** Вопрос */
  question: string;

  /** Тип вопроса */
  type: "radio" | "checkbox" | "text";

  /** К какому тесту относится вопрос */
  quiz_id: number;

  /** Ответы к вопросу */
  options?: QuestionOption[];

  /** Активный ли вопрос */
  active: string;

  image?: File;
}

export interface Quiz {
  /** ID теста */
  id?: number;

  /** Название теста */
  title: string;

  /** Максимальное количество попыток */
  max_attempt?: number;

  /** Максимальное количество баллов */
  max_score: Maybe<number>;

  /** Вопросы к тесту */
  questions?: Question[];

  /** Активен ли тест */
  visible: number;

  /** Показывать ли ответ */
  is_show_answer: number;

  /** Уведомлять ли администратора */
  is_notifications: number;

  /** Показывать отчёт в конце */
  is_report: number;

  /** Сортировка */
  type_sort: "scatter" | "order";

  /** Время на ответ в минутах */
  response_time?: number;

  /** Текст для превью */
  description?: string;

  /** Картинка превью */
  image?: File;

  /** Тип оценки теста */
  type_score?: "exam" | "pass" | "poll";

  /** Отношение оценок и процентов */
  ratio_percentages_and_score?: any;
}

export interface QuizAttemptData {
  /** ID попытки */
  id: number;

  /** ID теста */
  quiz_id: string;

  quiz: Quiz;

  /** Номер попытки по счёту */
  attempt?: string;

  /** Количество набранных баллов */
  score: Maybe<number>;

  /** ID пользователя */
  user_id?: string;

  /** Есть ли активная попытка */
  old: boolean;

  /** Насколько пройден тест */
  percent?: Maybe<number>;

  /** Закончен или нет.
   *
   * `"0"` — не закончен, `"1"` — закончен
   */
  is_finished?: string;

  created_at?: string;
  updated_at?: string;
}

export interface QuizWithAnswersData {
  /** ID попытки */
  id: number;

  /** Название вопроса */
  question: string;

  /** Тип вопроса */
  type: string;

  /** Ссылка на картинку */
  image: string;

  /** ID теста */
  quiz_id: number;

  /** Активный ли вопрос */
  active: number;

  /** Дата создания */
  created_at: number;

  /** Дата изменения */
  update_at: number;

  /** url */
  url: string;

  /** Опции вопросов */
  options: Options;

  /** Ответ */
  right: boolean;
}

export interface Options {
  /** ID вопроса */
  id: number;
  option: string;
  question_id: number;
  score: number;
  created_at: number;
  update_at: number;
  checked: boolean;
}

export interface currentQuizQuestionInterface {
  Question?: Question[];
  QuizAttempt?: QuizAttemptData[];
  QuizAnswer?: QuizWithAnswersData[];
  status?: string;
}
