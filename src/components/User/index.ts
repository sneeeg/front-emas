import UserGeneral from "./UserGeneral.vue";
import UserGeneralUser from "./UserGeneralUser.vue";
import UserAddress from "./UserAddress.vue";
import UserContract from "./UserContract.vue";
import UserContractUser from "./UserContractUser.vue";
import UserPassport from "./UserPassport.vue";
import UserCompany from "./UserCompany.vue";
import UserProgram from "./UserProgram.vue";
import UserActivity from "./UserActivity.vue";
import UserProgress from "./UserProgress.vue";
import UserCalendar from "./UserCalendar.vue";

export {
  UserGeneral,
  UserGeneralUser,
  UserAddress,
  UserContract,
  UserContractUser,
  UserPassport,
  UserCompany,
  UserProgram,
  UserActivity,
  UserProgress,
  UserCalendar
};
