export { default as QuizActionModal } from "./QuizActionModal.vue";
export { default as QuizQuestions } from "./QuizQuestions.vue";
export { default as QuizQuestionActionModal } from "./QuizQuestionActionModal.vue";
export { default as QuizQuestionOptionActionModal } from "./QuizQuestionOptionActionModal.vue";
export { default as QuizActionTab } from "./QuizActionTab.vue";
export { default as QuizQuestionActionTab } from "./QuizQuestionActionTab.vue";
