import { ReportLayoutsType, ReportTypes, ReportView } from "@/helpers/types";
const moment = require("moment");
import i18n from "@/i18n";
/**
 * Объект шаблонов для страницы `Отчёт`.
 *
 * В качестве ключа выступает `id`. Все `id` перечислены в enum `ReportTypes`.
 *
 * В качестве значения выступает массив из 2х или 3х объектов.
 * Объект содержит в себе заголовок карточки, тип карточки и перечисление контента.
 * Тип карточки может быть только одним из трёх:
 *    1. `table`   - таблица с выбором материала;
 *    2. `params`  - параметры отчёта;
 *    3. `result`  - параметры финального шага создания отчёта.
 *
 * Контент является уникальным для каждого типа карточек.
 */
export const reportLayouts: ReportLayoutsType = {
  [ReportTypes.QUIZ_SCORE_RESULTS]: {
    cards: [
      {
        title: "select_tests_dialogs_tasks",
        view: ReportView.TABLE,
        content: {
          getter: "getQuizzes",
          action: "fetchQuizzes",
          titleField: "title",
          selectType: "multiple"
        }
      },
      {
        title: "select_report_params",
        view: ReportView.PARAMS,
        content: {
          group: "Все группы",
          datepicker: null
        }
      },
      {
        title: String(i18n.t("create_report")),
        view: ReportView.RESULT,
        content: {
          requiredDatepicker: false
        }
      }
    ],
    result: {
      showInfo: true,
      table: {
        columns: [
          {
            field: "updated_at",
            label: "date_end",
            sortable: true,
            formatFn: value => {
              return moment(value).format("DD.MM.YYYY");
            }
          },
          {
            field: "name",
            label: "title_program",
            sortable: true
          },
          {
            field: "user_name",
            label: "user",
            sortable: true
          },
          {
            field: "is_finished",
            label: "status",
            sortable: true,
            formatFn: value => {
              if (value === "1") {
                return i18n.t("done");
              } else {
                return i18n.t("not_done");
              }
            }
          },
          {
            field: "percent",
            label: "percents",
            sortable: true,
            formatFn: value => {
              if (value) {
                return value + "%";
              }
            }
          },
          {
            field: "score",
            label: "score",
            sortable: true
          }
        ]
      }
    }
  },
  [ReportTypes.QUIZ_ANSWER_BREAKDOWN]: {
    cards: [
      {
        title: "select_test",
        view: ReportView.TABLE,
        content: {
          getter: "getQuizzes",
          action: "fetchQuizzes",
          titleField: "title",
          selectType: "single"
        }
      },
      {
        title: "select_report_params",
        view: ReportView.PARAMS,
        content: {
          group: String(i18n.t("all_groups")),
          datepicker: null
        }
      },
      {
        title: "create_report",
        view: ReportView.RESULT,
        content: {
          showReportBelow: false,
          requiredDatepicker: false
        }
      }
    ],
    result: {
      showInfo: false,
      type: "quizAnswerBreakdown",
      table: {
        columns: [
          {
            field: "option",
            label: "answer",
            width: "50%",
            sortable: false
          },
          {
            field: "percent",
            label: "percents",
            sortable: false,
            formatFn: value => Math.round(value) + "%"
          },
          {
            field: "count_answer",
            label: "answer_count",
            sortable: false
          }
        ]
      }
    }
  },
  [ReportTypes.LEARNING_PATH_SUMMARY]: {
    cards: [
      {
        title: "select_program_study",
        view: ReportView.TABLE,
        content: {
          getter: "getCourses",
          action: "fetchCourses",
          titleField: "name",
          selectType: "single"
        }
      },
      {
        title: "select_report_params",
        view: ReportView.PARAMS,
        content: {
          group: String(i18n.t("all_groups")),
          hideUnassigned: false
        }
      },
      {
        title: "create_report",
        view: ReportView.RESULT,
        content: {
          requiredDatepicker: false
        }
      }
    ],
    result: {
      showInfo: false,
      type: "learningPathSummary",
      table: {
        columns: [
          {
            field: "name",
            label: "title_program",
            sortable: true
          },
          {
            field: "user",
            label: "user",
            sortable: true
          },
          {
            field: "group_name",
            label: "group",
            sortable: false,
            formatFn: value => {
              return value ? value.join(", ") : value;
            }
          },
          {
            field: "status",
            label: "status",
            sortable: false,
            type: "number",
            formatFn: value => {
              return typeof value === "number" ? `${value}%` : value;
            }
          },
          {
            field: "date_end",
            label: "date_end",
            sortable: false,
            formatFn: value => {
              if (value) {
                return moment(value).format("DD.MM.YYYY");
              }
            }
          },
          {
            field: "date_plan",
            label: "end_to",
            sortable: false,
            formatFn: value => {
              if (value) {
                return moment(value.date).format("DD.MM.YYYY");
              }
            }
          }
        ]
      }
    }
  },
  [ReportTypes.USER_ACTIVITY]: {
    cards: [
      {
        title: "select_user",
        view: ReportView.TABLE,
        content: {
          getter: "getUsers",
          action: "fetchUsers",
          titleField: "name",
          selectType: "single"
        }
      },
      {
        title: "select_report_params",
        view: ReportView.PARAMS,
        content: {
          datepicker: null
        }
      },
      {
        title: "create_report",
        view: ReportView.RESULT,
        content: {
          requiredDatepicker: false
        }
      }
    ],
    result: {
      showInfo: false,
      type: "userActivity",
      table: {
        columns: [
          {
            field: "updated_at",
            label: "date",
            type: "date",
            dateInputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'",
            dateOutputFormat: "dd.MM.yyyy (HH:mm)",
            sortable: true
          },
          {
            field: "name",
            label: "material_title",
            sortable: true
          },
          {
            field: "user_id",
            label: "user",
            sortable: true
          }
        ]
      }
    }
  },
  [ReportTypes.LOOKED_NEWS]: {
    cards: [
      {
        title: "select_user",
        view: ReportView.TABLE,
        content: {
          getter: "getUsers",
          action: "fetchUsers",
          titleField: "name",
          type: "userActivity",
          selectType: "multiple"
        }
      },
      {
        title: "select_report_params",
        view: ReportView.PARAMS,
        content: {
          datepicker: null
        }
      },
      {
        title: "create_report",
        view: ReportView.RESULT,
        content: {
          requiredDatepicker: false
        }
      }
    ],
    result: {
      showInfo: false,
      type: "userActivity",
      table: {
        columns: [
          {
            field: "title",
            label: "title",
            formatFn: value => {
              if (value) {
                return value;
              } else {
                return "Нет названия";
              }
            }
          },
          {
            field: "user_name",
            label: "user",
            sortable: true
          },
          {
            field: "date_news",
            label: "date_news",
            sortable: true
          },
          {
            field: "date_view",
            label: "date_view",
            sortable: true
          }
        ]
      }
    }
  },
  [ReportTypes.SUBMITTED_PROJECT]: {
    cards: [
      {
        title: "select_user",
        view: ReportView.TABLE,
        content: {
          getter: "getUsers",
          action: "fetchUsers",
          titleField: "name",
          type: "submittedProject",
          selectType: "multiple"
        }
      },
      {
        title: "select_report_params",
        view: ReportView.PARAMS,
        content: {
          datepicker: null
        }
      },
      {
        title: "create_report",
        view: ReportView.RESULT,
        content: {
          requiredDatepicker: false
        }
      }
    ],
    result: {
      showInfo: false,
      type: "submittedProject",
      table: {
        columns: [
          {
            field: "updated_at",
            label: "date",
            type: "date",
            dateInputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'",
            dateOutputFormat: "dd.MM.yyyy (HH:mm)",
            sortable: true
          },
          {
            field: "name",
            label: "material_title",
            sortable: true
          },
          {
            field: "user_id",
            label: "user",
            sortable: true
          }
        ]
      }
    }
  },
  [ReportTypes.TRAFFIC_LIGHTS]: {
    cards: [
      //{
      //  title: String(i18n.t("select_user")),
      //  view: ReportView.TABLE,
      //  content: {
      //    getter: "getUsers",
      //    action: "fetchUsers",
      //    titleField: "name",
      //    type: "trafficLights",
      //    selectType: "single"
      //  }
      //},
      {
        title: "select_report_params",
        view: ReportView.PARAMS,
        content: {
          datepicker: null,
          step: [
            { value: "day", title: i18n.t("day") },
            { value: "week", title: i18n.t("week") },
            { value: "month", title: i18n.t("month") }
          ],
          defaultStep: { value: "week", title: String(i18n.t("week")) },
          requiredDatepicker: true
        }
      },
      {
        title: "create_report",
        view: ReportView.RESULT,
        content: {
          requiredDatepicker: true
        }
      }
    ],
    result: {
      showInfo: false,
      type: "trafficlights",
      table: {
        columns: [
          {
            field: "created_at",
            label: "date_end",
            sortable: true
          },
          {
            field: "name",
            label: "title_program",
            sortable: true
          },
          {
            field: "user_id",
            label: "user",
            sortable: true
          }
        ]
      }
    }
  }
};
