export const dateRangeShortcuts = [
  {
    text: "ALL_TIME",
    onClick: () => {
      return [0, 0];
    }
  },
  {
    text: "YEAR",
    onClick: () => {
      const startDate = new Date();
      const endDate = new Date();
      const currentYear: number = new Date().getFullYear();

      startDate.setTime(+new Date(`1-1-${currentYear}`));

      return [startDate, endDate];
    }
  },
  {
    text: "QUARTER",
    onClick: () => {
      const startDate = new Date();
      const endDate = new Date();
      const currentMonth = new Date().getMonth() + 1;
      const currentDay: number = new Date().getDate();
      const currentYear: number = new Date().getFullYear();
      let startMonth = 0;

      switch (currentMonth) {
        case 1:
        case 2:
        case 3:
          startMonth = 1;
          break;
        case 4:
        case 5:
        case 6:
          startMonth = 3;
          break;
        case 7:
        case 8:
        case 9:
          startMonth = 6;
          break;
        case 10:
        case 11:
        case 12:
          startMonth = 9;
          break;
        default:
          break;
      }

      startDate.setTime(+new Date(`${startMonth}-1-${currentYear}`));
      endDate.setTime(
        +new Date(`${currentMonth}-${currentDay}-${currentYear}`)
      );
      return [startDate, endDate];
    }
  },
  {
    text: "MONTH",
    onClick: () => {
      const startDate = new Date();
      const endDate = new Date();
      const currentMonth: number = new Date().getMonth();
      const currentYear: number = new Date().getFullYear();

      startDate.setTime(+new Date(`${currentMonth + 1}-1-${currentYear}`));

      return [startDate, endDate];
    }
  },
  {
    text: "WEEK",
    onClick: () => {
      const startDate = new Date();
      const endDate = new Date();
      startDate.setTime(+startDate.getTime() - 3600 * 1000 * 24 * 7);
      return [startDate, endDate];
    }
  },
  {
    text: "YESTERDAY",
    onClick: () => {
      const date = new Date();
      date.setTime(+date.getTime() - 3600 * 1000 * 24);
      return [date, date];
    }
  }
];
