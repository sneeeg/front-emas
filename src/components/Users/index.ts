import UserGeneral from "./UserGeneral.vue";
import UserAddress from "./UsersTable.vue";
import UserContract from "./UserContract.vue";
import UserPassport from "./UserPassport.vue";
import UserCompany from "./UserCompany.vue";

export { UserGeneral, UserAddress, UserContract, UserPassport, UserCompany };
