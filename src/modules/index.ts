import Avatar from "./Avatar.vue";
import Header from "./Header.vue";
import Logo from "./Logo.vue";
import Menu from "./Menu.vue";
import MenuAdmin from "./MenuAdmin.vue";
import Sidebar from "./Sidebar.vue";
import SidebarAdmin from "./SidebarAdmin.vue";

export { Avatar, Header, Logo, Menu, MenuAdmin, Sidebar, SidebarAdmin };
