import Vue from "vue";
import App from "./App.vue";
import router from "@/router";
import store from "@/store";
import "vue-awesome/icons";
import Icon from "vue-awesome/components/Icon";
import "./registerServiceWorker";
import VModal from "vue-js-modal";
import Notifications from "vue-notification";
import VueEvents from "vue-events";
import VueGapi from "vue-gapi";
const VueUploadComponent = require("vue-upload-component");
import i18n from "@/i18n";
import VueI18n from "vue-i18n";
import VueCookies from "vue-cookies";
import { ValidationObserver } from "vee-validate";
import { ValidationProvider } from "vee-validate";

// Register it globally
// main.js or any entry file.
Vue.component("ValidationObserver", ValidationObserver);
Vue.component("ValidationProvider", ValidationProvider);
Vue.use(VueCookies);
Vue.use(VueI18n);
import { LicenseManager } from "@ag-grid-enterprise/core";
LicenseManager.setLicenseKey(
  "IRDEVELOPERS_COM_NDEwMjM0NTgwMDAwMA==f08aae16269f416fe8d65bbf9396be5f"
);
Vue.component("file-upload", VueUploadComponent);
Vue.use(VueGapi, {
  apiKey: "AIzaSyANbhrQkYP4QS-oM0EfTS1qY9-0sIH2NuE",
  clientId: "<YOUR_CLIENT_ID>.apps.googleusercontent.com",
  discoveryDocs: ["https://sheets.googleapis.com/$discovery/rest?version=v4"],
  scope: "https://www.googleapis.com/auth/spreadsheets"
});
const moment = require("moment");
require("moment/locale/ru");
Vue.use(require("vue-moment"), {
  moment
});

Vue.use(VueEvents);
Vue.use(Notifications);
Vue.use(VModal, { componentName: "VModal", dialog: true });
Vue.component("Icon", Icon);
Vue.config.productionTip = false;
new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount("#app");
