import { VuexModule, Module, Action, Mutation } from "vuex-module-decorators";
import API from "@/api";
import { handlerError } from "@/helpers/utils/handlerError";
import { handlerSuccess } from "@/helpers/utils/handlerSuccess";
import { Maybe } from "@/helpers/types";
import i18n from "@/i18n";
import uniqBy from "lodash/uniqBy";
@Module
class SettingsModule extends VuexModule {
  public settings: Maybe<any[]> = null;
  public setting: Maybe<any[]> = null;
  public settingSocial: Maybe<any[]> = null;

  @Mutation
  private saveSettings(payload: any[]) {
    this.settings = payload;
  }
  @Mutation
  private saveSetting(payload: any[]) {
    this.setting = payload;
  }
  @Mutation
  private saveSettingSocial(payload: any[]) {
    this.settingSocial = payload;
  }

  @Action
  public async fetchSettings(): Promise<void> {
    try {
      const { data } = await API.settings.getSettings();
      this.context.commit("saveSettings", data);
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async fetchSetting(id: number): Promise<void> {
    try {
      const { data } = await API.settings.getSetting(id);
      this.context.commit("saveSetting", data);
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async fetchSettingSocial(): Promise<void> {
    try {
      const { data } = await API.settings.getSettingSocial();
      this.context.commit("saveSettingSocial", data);
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async editSetting(params: any): Promise<void> {
    try {
      await API.settings.editSetting(params);
      handlerSuccess({
        title: String(i18n.t("setting_edit")),
        text: String(i18n.t("setting_edit_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  public get getSettings() {
    return this.settings;
  }
  public get getSetting() {
    return this.setting;
  }
  public get getSettingSocial() {
    return this.settingSocial;
  }
}

export { SettingsModule };
