import API from "@/api";
import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators";
import i18n from "@/i18n";
import { Maybe, UserRole, UserInterface } from "@/helpers/types";
import { handlerError } from "@/helpers/utils/handlerError";
import { handlerSuccess } from "@/helpers/utils/handlerSuccess";
import { makeFormData, DateToString } from "@/helpers/utils/functions";

@Module
class ProfileModule extends VuexModule {
  public profile: Maybe<UserInterface> = null;
  public view: string = "";

  @Mutation
  private setProfile(payload: Maybe<UserInterface>) {
    this.profile = payload;
  }
  @Mutation
  private setView(payload: string) {
    this.view = payload;
  }

  @Action
  public async fetchProfile(): Promise<void> {
    try {
      const { data } = await API.user.getProfile();
      const user = { ...data, roles: data.cusRoles };

      this.context.commit("setProfile", user);
    } catch (error) {
      await this.context.dispatch("logout");
      handlerError(error as any);
    }
  }
  @Action
  public async setCurrentView(view: string): Promise<void> {
    this.context.commit("setView", view);
  }
  @Action
  public async editProfile(user: UserInterface): Promise<void> {
    user.date_termination = DateToString(user.date_termination);
    user.d_date = DateToString(user.d_date);
    user.date_block = DateToString(user.date_block);
    user.date_start = DateToString(user.date_start);
    user.passport_number_when = DateToString(user.passport_number_when);
    user.birthday_date = DateToString(user.birthday_date);
    try {
      await API.profile.editProfile(user);
      handlerSuccess({
        title: String(i18n.t("edit_profile")),
        text: String(i18n.t("edit_profile_desc"))
      });
    } catch (error) {
      console.log("error", error);
      handlerError(error as any);
    }
  }
  @Action
  public async setProfileAvatar(user: UserInterface): Promise<void> {
    try {
      const fd = makeFormData({
        avatar: user.avatar
      });

      await API.profile.setProfileAvatar(fd);
      handlerSuccess({
        title: String(i18n.t("add_avatar")),
        text: String(i18n.t("add_avatar_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  public get getProfile(): Maybe<UserInterface> {
    return this.profile;
  }
  public get getView() {
    return this.view;
  }

  public get getRole(): UserRole | undefined {
    return this.profile?.roles?.[0];
  }

  public get isSuperAdmin(): boolean {
    if (this.profile?.roles?.includes(UserRole.superadmin)) {
      return true;
    }

    return false;
  }
  public get isAdmin(): boolean {
    if (this.profile?.roles?.includes(UserRole.admin)) {
      return true;
    }

    return false;
  }
  public get isNew(): boolean {
    if (
      this.profile?.programView &&
      this.profile?.format &&
      this.profile?.priceSelected &&
      this.profile?.currency &&
      this.profile?.payment &&
      !this.profile?.is_start
    ) {
      return false;
    }
    return true;
  }
  public get isModerator(): boolean {
    if (this.profile?.roles?.includes(UserRole.moderator)) {
      return true;
    }

    return false;
  }

  public get isStudent(): boolean {
    if (this.profile?.roles?.includes(UserRole.student)) {
      return true;
    }

    return false;
  }
}

export { ProfileModule };
