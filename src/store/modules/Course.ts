import { Action, Module, Mutation, VuexModule } from "vuex-module-decorators";

import API from "@/api";
import {
  ChapterInterface,
  CourseInterface,
  GroupInterface,
  Maybe,
  UserInterface,
  DataInterface
} from "@/helpers/types";
import { handlerSuccess } from "@/helpers/utils/handlerSuccess";
import { handlerError } from "@/helpers/utils/handlerError";
import { URL } from "@/helpers/constants";
import router from "@/router";
import Vue from "vue";
import { RequestInterface } from "@/helpers/types/common";
import i18n from "@/i18n";

@Module
class CourseModule extends VuexModule {
  public saved: { [key: number]: CourseInterface } = {};
  public editableChapter?: Maybe<ChapterInterface> = null;
  public userCourses?: Maybe<DataInterface<CourseInterface[]>> = null;

  @Mutation
  private saveCourse(payload: CourseInterface) {
    this.saved[payload.id as number] = payload;
  }

  @Mutation
  public saveUserCourse(payload: Maybe<DataInterface<CourseInterface[]>>) {
    this.userCourses = payload;
  }

  @Action
  public async fetchCourseForUser(id: number): Promise<void> {
    const {
      data
    }: RequestInterface<DataInterface<
      CourseInterface[]
    >> = await API.course.getUserCourseAdmin(id);
    this.context.commit("saveUserCourse", data);
  }

  public get getCourseForUser() {
    return this.userCourses;
  }

  @Mutation
  public addChapter(payload: ChapterInterface) {
    this.saved[payload.program_id as number].chapter?.push(payload);
  }

  @Mutation
  public setEditableChapter(payload: ChapterInterface) {
    this.editableChapter = payload;
  }

  @Mutation
  public updateChapter(payload: ChapterInterface) {
    const { program_id, id } = payload;
    const { chapter } = this.saved[program_id];

    if (chapter) {
      const index = chapter.findIndex(c => c.id === id);

      if (chapter[index]) {
        Vue.set(chapter, index, payload);
      }
    }
  }

  @Mutation
  public deleteChapterItem(payload: ChapterInterface) {
    const list = this.saved[payload.program_id as number].chapter;
    const filtered = list?.filter(item => item.id !== payload.id);
    if (filtered) {
      this.saved[payload.program_id as number].chapter = filtered;
    }
  }

  @Action
  public async fetchCourse(id: number): Promise<void> {
    try {
      const { data } = await API.course.getUserCourse(id);

      this.context.commit("saveCourse", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async fetchCourseAdmin(id: number): Promise<void> {
    try {
      const { data } = await API.course.getCourse(id);

      this.context.commit("saveCourse", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async addUserToCourse({
    user,
    id
  }: {
    user: UserInterface;
    id: number;
  }): Promise<void> {
    try {
      const params = {
        users: [user.id]
      };
      const { data } = await API.course.setUsers(id, params);
      this.context.commit("saveCourse", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async deleteUserFromCourse({
    user,
    id
  }: {
    user: UserInterface;
    id: number;
  }): Promise<void> {
    try {
      const params = {
        users: [user]
      };
      const { data } = await API.course.deleteUsers(id, params);
      this.context.commit("saveCourse", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async addGroupToCourse({
    group,
    id
  }: {
    group: GroupInterface;
    id: number;
  }): Promise<void> {
    try {
      const params = {
        groups: [group.id]
      };
      const { data } = await API.course.setGroups(id, params);
      this.context.commit("saveCourse", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async deleteGroupFromCourse({
    group,
    id
  }: {
    group: GroupInterface;
    id: number;
  }): Promise<void> {
    try {
      const params = {
        groups: [group.id]
      };
      const { data } = await API.course.deleteGroups(id, params);
      this.context.commit("saveCourse", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async createCourse(course: CourseInterface): Promise<void> {
    try {
      const { data } = await API.course.createCourse(course);
      await router.push(`${URL.COURSES}/${data.id}/edit`);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async editCourse(course: CourseInterface): Promise<void> {
    try {
      await API.course.editCourse(course.id, course);
      handlerSuccess({
        title: String(i18n.t("program_is_update")),
        text: String(i18n.t("program_is_update_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async createChapter(params: CourseInterface): Promise<void> {
    try {
      await API.course.createChapter(params);
      handlerSuccess({
        title: String(i18n.t("chapter_is_add")),
        text: String(i18n.t("chapter_is_add_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async editChapter(chapter: ChapterInterface): Promise<void> {
    try {
      await API.course.editChapter(chapter.id, chapter);
      this.context.commit("setEditableChapter", null);
      handlerSuccess({
        title: String(i18n.t("chapter_is_update")),
        text: String(i18n.t("chapter_is_update_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async sortChapterMedia(params: any): Promise<void> {
    const { id } = params;
    try {
      const { data } = await API.course.sortChapterMedia(id, params);
      this.context.commit("updateChapter", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async sortChapter(params: any): Promise<void> {
    const { id } = params;
    try {
      const { data } = await API.course.sortChapter(id, params);
      this.context.commit("saveCourse", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async sortChapterQuizzes(params: any): Promise<void> {
    const { id } = params;
    try {
      const { data } = await API.course.sortChapterQuizzes(id, params);
      this.context.commit("updateChapter", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async deleteChapter(chapter: ChapterInterface): Promise<void> {
    try {
      await API.course.deleteChapter(chapter.id);
      this.context.commit("deleteChapterItem", chapter);
      handlerSuccess({
        title: String(i18n.t("chapter_is_delete")),
        text: String(i18n.t("chapter_is_delete_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async attachMediaToChapter(params: any): Promise<void> {
    const { id } = params;
    try {
      const { data } = await API.course.attachMediaToChapter(id, params);
      this.context.commit("updateChapter", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async attachQuizzesToChapter(params: any): Promise<void> {
    const { id } = params;
    try {
      const { data } = await API.course.attachQuizzesToChapter(id, params);
      this.context.commit("updateChapter", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async deleteQuizzChapter(params: any): Promise<void> {
    const { id } = params;
    try {
      const { data } = await API.course.deleteQuizzChapter(id, params);
      this.context.commit("updateChapter", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async deleteMediaChapter(params: any): Promise<void> {
    const { id } = params;
    try {
      const { data } = await API.course.deleteMediaChapter(id, params);
      this.context.commit("updateChapter", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async cloneCourse(id: number): Promise<void> {
    try {
      await API.course.cloneCourse(id);
      handlerSuccess({
        title: String(i18n.t("course_is_clone")),
        text: String(i18n.t("course_is_clone_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  public get getCourse(): (id: number) => CourseInterface {
    return (id: number): CourseInterface => {
      return this.saved[id];
    };
  }

  public get getEditableChapter() {
    return this.editableChapter;
  }
}

export { CourseModule };
