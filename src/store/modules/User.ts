import { VuexModule, Module, Action, Mutation } from "vuex-module-decorators";
import API from "@/api";
import i18n from "@/i18n";
import { UserInterface } from "@/helpers/types";
import { handlerSuccess } from "@/helpers/utils/handlerSuccess";
import { handlerError } from "@/helpers/utils/handlerError";
import { handlerErrorPass } from "@/helpers/utils/handlerErrorPass";
import { makeFormData, DateToString } from "@/helpers/utils/functions";

@Module
class UserModule extends VuexModule {
  public saved: { [key: number]: UserInterface } = {};
  public userPlan: any[] = [];
  public errorUserPlan: boolean = false;
  public userRating: any[] = [];
  public emasPrograms: any[] = [];
  public socialActivity: any[] = [];
  public payment: any[] = [];
  public video: string = "";

  @Mutation
  errorUserPlanMut(payload: boolean) {
    this.errorUserPlan = payload;
  }

  @Mutation
  saveUser(payload: UserInterface) {
    this.saved[payload.id as number] = payload;
  }

  @Mutation
  saveUserPlan(payload: any) {
    this.userPlan = payload;
  }

  @Mutation
  saveUserRating(payload: any) {
    this.userRating = payload;
  }

  @Mutation
  saveEmasPrograms(payload: any) {
    this.emasPrograms = payload;
  }

  @Mutation
  saveSocialActivity(payload: any) {
    this.socialActivity = payload;
  }

  @Mutation
  saveVideo(payload: any) {
    this.video = payload;
  }

  @Mutation
  savePayment(payload: any) {
    this.payment = payload;
  }

  get getterUserPlan() {
    return this.userPlan;
  }

  get getterEmasPrograms() {
    return this.emasPrograms;
  }

  get getterSocialActivity() {
    return this.socialActivity;
  }

  get getUserRating() {
    return this.userRating;
  }

  get getterVideo() {
    return this.video;
  }

  get getPayment() {
    return this.payment;
  }

  get getUser(): (id: number) => UserInterface {
    return (id: number): UserInterface => {
      return this.saved[id];
    };
  }

  get errorUserPlanGet() {
    return this.errorUserPlan;
  }

  @Action
  public async fetchUser(id: number): Promise<void> {
    try {
      const { data } = await API.user.getUser(id);

      this.context.commit("saveUser", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async getUserPlan(): Promise<void> {
    try {
      const { data } = await API.user.getUserPlan();

      this.context.commit("errorUserPlanMut", false);
      this.context.commit("saveUserPlan", data);
    } catch (error) {
      this.context.commit("errorUserPlanMut", true);
      handlerError(error as any);
    }
  }

  @Action
  public async getAdminUserPlan(id: number): Promise<void> {
    try {
      const { data } = await API.user.getAdminUserPlan(id);

      this.context.commit("saveUserPlan", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async getEmasPrograms(): Promise<void> {
    try {
      const { data } = await API.user.getEmasPrograms();

      this.context.commit("saveEmasPrograms", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async getVideo(id: number): Promise<void> {
    this.context.commit("saveVideo", null); // Update getter

    try {
      const { data } = await API.user.getVideo(id);

      this.context.commit("saveVideo", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async getUserPayment(): Promise<void> {
    try {
      const { data } = await API.user.getUserPayment();

      this.context.commit("savePayment", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async resetPayment(id: number): Promise<void> {
    try {
      await API.user.resetPayment(id);

      handlerSuccess({
        title: String(i18n.t("reset_payment_text")),
        text: ""
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async getRating(): Promise<void> {
    try {
      const { data } = await API.user.getRating();

      this.context.commit("saveUserRating", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async getSocialActivity(user: UserInterface): Promise<void> {
    try {
      const { data } = await API.user.getSocialActivity(user.id);

      this.context.commit("saveSocialActivity", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action({ rawError: true })
  public async changePass(params: any): Promise<void> {
    try {
      await API.user.changePass(params);

      handlerSuccess({
        title: String(i18n.t("change_pass")),
        text: ""
      });
    } catch (error) {
      handlerErrorPass(error as any);
    }
  }

  @Action
  public async sendWelcomeEmail(id: number): Promise<void> {
    try {
      await API.user.sendWelcomeEmail(id);

      handlerSuccess({
        title: String(i18n.t("send_email_welcome_success")),
        text: ""
      });
    } catch (error) {
      handlerErrorPass(error as any);
    }
  }

  @Action
  public async sendEmail(params: any): Promise<void> {
    try {
      await API.users.sendEmail(params);

      handlerSuccess({
        title: String(i18n.t("send_mail")),
        text: ""
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async setScore(params: any): Promise<void> {
    console.log(1);

    // try {
    //   await API.user.setScore(params.id, params);

    //   handlerSuccess({
    //     title: String(i18n.t("set_score")),
    //     text: ""
    //   });
    // } catch (error) {
    //   handlerError(error as any);
    // }
  }

  @Action
  public async editUser(user: UserInterface): Promise<void> {
    user.date_termination = DateToString(user.date_termination);
    user.d_date = DateToString(user.d_date);
    user.date_block = DateToString(user.date_block);
    user.date_start = DateToString(user.date_start);
    user.passport_number_when = DateToString(user.passport_number_when);
    user.birthday_date = DateToString(user.birthday_date);

    try {
      await API.user.editUser(user.id, user);

      handlerSuccess({
        title: String(i18n.t("edit_user")),
        text: String(i18n.t("edit_user_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async editUserActivity(params: any): Promise<void> {
    try {
      await API.user.editUserActivity(params.id, params.social, params);

      handlerSuccess({
        title: String(i18n.t("edit_activity")),
        text: ""
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async deleteUser(id: number): Promise<void> {
    try {
      await API.user.deleteUser(id);

      handlerSuccess({
        title: String(i18n.t("delete_user")),
        text: String(i18n.t("delete_user_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async setUserAvatar(user: UserInterface): Promise<void> {
    try {
      const fd = makeFormData({
        avatar: user.avatar
      });

      await API.user.setUserAvatar(user.id, fd);

      handlerSuccess({
        title: String(i18n.t("add_avatar")),
        text: String(i18n.t("add_avatar_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
}

export { UserModule };
