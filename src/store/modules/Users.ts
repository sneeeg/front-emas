import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators";
import i18n from "@/i18n";
import API from "@/api";
import { Maybe, DataInterface, UserInterface } from "@/helpers/types";
import { handlerError } from "@/helpers/utils/handlerError";
import { handlerSuccess } from "@/helpers/utils/handlerSuccess";

@Module
class UsersModule extends VuexModule {
  public users: Maybe<DataInterface<UserInterface[]>> = null;

  @Mutation
  private setUsers(payload: Maybe<DataInterface<UserInterface[]>>) {
    this.users = payload;
  }
  public usersSlug: Maybe<DataInterface<UserInterface[]>> = null;

  @Mutation
  private setUsersSlug(payload: Maybe<DataInterface<UserInterface[]>>) {
    this.usersSlug = payload;
  }

  @Action
  public async fetchUsers(params = ""): Promise<void> {
    try {
      const { data } = await API.user.getUsers(params);
      this.context.commit("setUsers", data);
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async fetchUsersSlug(params = ""): Promise<void> {
    try {
      const { data } = await API.user.getUsersSlug(params);
      this.context.commit("setUsersSlug", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async addUsers(params: {
    email: string[];
    message?: string;
  }): Promise<void> {
    try {
      await API.user.addUsers(params);
      handlerSuccess({
        title: String(i18n.t("add_users")),
        text: String(i18n.t("add_users_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async deleteUsers(params: any): Promise<void> {
    try {
      await API.users.deleteUsers(params);
      handlerSuccess({
        title: String(i18n.t("add_users")),
        text: String(i18n.t("add_users"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async checkPayments(params: any): Promise<void> {
    try {
      await API.users.checkPayments(params);
      handlerSuccess({
        title: String(i18n.t("check_payment")),
        text: ""
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async blockUsers(params: any): Promise<void> {
    try {
      await API.users.blockUsers(params);
      handlerSuccess({
        title: String(i18n.t("block_users")),
        text: String(i18n.t("block_users_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async unblockUsers(params: any): Promise<void> {
    try {
      await API.users.unblockUsers(params);
      handlerSuccess({
        title: String(i18n.t("unblock_users")),
        text: String(i18n.t("unblock_users_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  public get getUsers(): Maybe<DataInterface<UserInterface[]>> {
    return this.users;
  }
  public get gettertUsersSlug(): Maybe<DataInterface<UserInterface[]>> {
    return this.usersSlug;
  }
}

export { UsersModule };
