import { AuthModule } from "./Auth";
import { ProfileModule } from "./Profile";
import { CoursesModule } from "./Courses";
import { CoursesDoneModule } from "./CoursesDone";
import { CourseModule } from "./Course";
import { DocumentsModule } from "./Documents";
import { UsersModule } from "./Users";
import { UserModule } from "./User";
import { GroupsModule } from "./Groups";
import { RatingModule } from "./Rating";
import { TemplatesModule } from "./Templates";
import { FileManagerModule } from "./FileManager";
import { ReportsModule } from "./Reports";
import { ReportModule } from "./Report";
import { QuizModule } from "./Quiz";
import { NewsModule } from "./News";
import { MediaModule } from "./Media";
import { NetworkModule } from "./Networking"
import { PriceModule } from "./Price";
import { ProjectModule } from "./Project";
import { SettingsModule } from "./Settings";

export const modules = {
  AuthModule,
  ProfileModule,
  CoursesModule,
  CoursesDoneModule,
  CourseModule,
  DocumentsModule,
  UsersModule,
  UserModule,
  GroupsModule,
  RatingModule,
  TemplatesModule,
  FileManagerModule,
  ReportsModule,
  ReportModule,
  QuizModule,
  NewsModule,
  MediaModule,
  NetworkModule,
  PriceModule,
  ProjectModule,
  SettingsModule
};
