import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators";

import API from "@/api";
import store from "@/store";
import { Maybe, CourseInterface, DataInterface } from "@/helpers/types";
import { handlerSuccess } from "@/helpers/utils/handlerSuccess";
import { handlerError } from "@/helpers/utils/handlerError";

@Module
class CoursesDoneModule extends VuexModule {
  public coursesDone: Maybe<DataInterface<CourseInterface[]>> = null;

  @Mutation
  private setCoursesDone(payload: Maybe<DataInterface<CourseInterface[]>>) {
    this.coursesDone = payload;
  }

  @Action
  public async fetchCoursesDone(): Promise<void> {
    
    try {
      const response = await API.courses.getUserCourses("done=true");
      this.context.commit("setCoursesDone", response);
    } catch (error) {
      handlerError(error as any);
    }
  }

  public get getCoursesDone(): Maybe<DataInterface<CourseInterface[]>> {
    return this.coursesDone;
  }
}

export { CoursesDoneModule };
