import { VuexModule, Module, Action, Mutation } from "vuex-module-decorators";
import API from "@/api";
import i18n from "@/i18n";
import { Maybe } from "@/helpers/types";
import { handlerError } from "@/helpers/utils/handlerError";
import { handlerSuccess } from "@/helpers/utils/handlerSuccess";

@Module
class NetworkModule extends VuexModule {
  public allNetworking: any = null;
  public searchResult: any = null;

  public get getAllNetworking() {
    return this.allNetworking;
  }

  public get getSearchResult() {
    return this.searchResult;
  }

  @Mutation
  private saveAllNetworking(payload: any) {
    this.allNetworking = null;
    this.allNetworking = payload;
  }

  @Mutation
  private saveSearchResult(payload: any) {
    this.searchResult = null;
    this.searchResult = payload;
  }

  @Action
  public async addAllNetworking(): Promise<void> {
    try {
      const { data } = await API.networking.getAllNetworkingFields();
      this.context.commit("saveAllNetworking", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async saveAllNetworkingFileds(params: any): Promise<void> {
    try {
      await API.networking.saveAllNetworkingFileds(params);
      this.context.dispatch("addAllNetworking");
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async toSearchNetwororking(params: any): Promise<void> {
    try {
      const { data } = await API.networking.toSearchNetwororking(params);
      this.context.commit("saveSearchResult", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async saveNetworkingStatus(status: any) {
    try {
      const { data } = await API.networking.saveNetworkingStatus(status);
    } catch (error) {
      handlerError(error as any);
    }
  }
}

export { NetworkModule };
