import {
  Action,
  Module,
  Mutation,
  VuexModule,
  config
} from "vuex-module-decorators";
import API from "@/api";
import i18n from "@/i18n";
config.rawError = true;
import { Maybe, DataInterface, Scalars } from "@/helpers/types";
import {
  Question,
  QuestionOption,
  QuestionUserAnswer,
  Quiz,
  QuizWithAnswersData,
  QuizAttemptData
} from "@/helpers/types/quiz";
import { RequestInterface } from "@/helpers/types/common";
import { handlerSuccess } from "@/helpers/utils/handlerSuccess";
import { handlerError } from "@/helpers/utils/handlerError";
import { makeQueryURL } from "@/helpers/utils/functions";

@Module
class QuizModule extends VuexModule {
  // QUIZZES --------------------------------------------------
  public quizzes: Maybe<DataInterface<Quiz[]>> = null;

  @Mutation
  private saveQuizzes(payload: Maybe<DataInterface<Quiz[]>>) {
    this.quizzes = payload;
  }

  @Action
  public async fetchQuizzes(params = ""): Promise<void> {

    try {
      const {
        data
      }: RequestInterface<DataInterface<Quiz[]>> = await API.quiz.getAllQuizzes(
        params
      );
      this.context.commit("saveQuizzes", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  public get getQuizzes() {
    return this.quizzes;
  }

  // QUIZ --------------------------------------------------

  public quiz: Maybe<Quiz> = null;
  public quizError: Array<any> = [];
  public quizAttemptData: Maybe<QuizAttemptData> = null;
  public currentQuizQuestion: Maybe<Question> = null;

  @Mutation
  private saveQuiz(payload: Maybe<Quiz>, errorName: any) {
    this.quiz = payload;
    this.quizError = errorName;
  }
  @Mutation
  private saveQuizError(errorName: Array<any>) {
    this.quizError = errorName;
  }

  @Mutation
  private saveQuizAttemptData(payload: Maybe<QuizAttemptData>) {
    this.quizAttemptData = payload;
  }

  @Mutation
  private saveQuizCurrentQuiz(payload: Maybe<Question>) {
    this.currentQuizQuestion = payload;
  }

  @Action
  public async addQuiz(quiz: Quiz): Promise<void> {
    try {
      const { data }: RequestInterface<Quiz> = await API.quiz.addQuiz(quiz);
      this.context.commit("saveQuiz", data);
      handlerSuccess({
        title: String(i18n.t("add_quiz_text")),
        text: ""
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async deleteQuizzes(params: any): Promise<void> {
    try {
      await API.quiz.deleteQuizzes(params);
      handlerSuccess({
        title: String(i18n.t("delete_quizzes")),
        text: String(i18n.t("delete_quizzes_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async fetchQuiz(id: Quiz["id"]): Promise<void> {

    try {
      const { data }: RequestInterface<Quiz> = await API.quiz.getQuiz(id);
      this.context.commit("saveQuiz", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async fetchQuizAttemptData(id: Quiz["id"]): Promise<void> {
    try {
      const {
        data
      }: RequestInterface<QuizAttemptData> = await API.quiz.getQuizAttemptData(
          id
      );
      this.context.commit("saveQuizAttemptData", data);
    } catch (error) {
      handlerError(error as any);
      this.context.commit("saveQuizError", error);
    }
  }

  @Action
  public async fetchQuizNewAttemptData(id: Quiz["id"]): Promise<void> {

    try {
      const {
        data
      }: RequestInterface<QuizAttemptData> = await API.quiz.getQuizNewAttemptData(
        id
      );
      this.context.commit("saveQuizAttemptData", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async fetchQuizQuestion(quiz: QuizAttemptData): Promise<void> {

    try {
      const { data }: RequestInterface<Question> = await API.quiz.getQuizQuestion(
        quiz.quiz_id,
        quiz.id
      );
      this.context.commit("saveQuizCurrentQuiz", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async updateQuiz(quiz: Partial<Quiz>): Promise<void> {
    try {
      const { data }: RequestInterface<Quiz> = await API.quiz.updateQuiz(
        quiz.id,
        quiz
      );
      this.context.commit("saveQuiz", data);
      handlerSuccess({
        title: String(i18n.t("update_quiz")),
        text: ""
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async deleteQuiz(quiz: Quiz): Promise<void> {
    try {
      const { data }: RequestInterface<Quiz> = await API.quiz.deleteQuiz(
        quiz.id
      );
      this.context.commit("saveQuiz", data);
      handlerSuccess({
        title: String(i18n.t("delete_quiz_text")),
        text: ""
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  public get getQuiz() {
    return this.quiz;
  }
  public get getQuizError() {
    return this.quizError;
  }

  public get getQuizAttemptData() {
    return this.quizAttemptData;
  }

  public set setNewAttemptData(value: QuizAttemptData) {
    this.quizAttemptData = value;
  }

  public get getCurrentQuizQuestion() {
    return this.currentQuizQuestion;
  }

  // QUESTIONS --------------------------------------------------

  public questions: Question[] = [];

  @Mutation
  private saveQuestions(payload: Question[]) {
    this.questions = payload;
  }

  @Action
  public async fetchQuestions(params: Pick<Quiz, "id">): Promise<void> {
    const queryParams = makeQueryURL(params);
    const {
      data
    }: RequestInterface<Question[]> = await API.quiz.getAllQuestions(
      queryParams
    );
    this.context.commit("saveQuestions", data);
  }

  public get getQuestions(): Question[] {
    return this.questions;
  }

  // QUESTION --------------------------------------------------

  public question: Maybe<Question> = null;

  @Mutation
  private saveQuestion(payload: Maybe<Question>) {
    this.question = payload;
  }

  @Action
  public async addQuestion(question: Question): Promise<void> {
    try {
      const { data }: RequestInterface<Question> = await API.quiz.addQuestion(
        question
      );
      this.context.commit("saveQuestion", data);
      handlerSuccess({
        title: String(i18n.t("add_question_text")),
        text: ""
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async fetchQuestion(question: Question): Promise<void> {
    try {
      const { data }: RequestInterface<Question> = await API.quiz.getQuestion(
        question
      );
      this.context.commit("saveQuestion", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async updateQuestion(question: Partial<Question>): Promise<void> {
    try {
      const {
        data
      }: RequestInterface<Question> = await API.quiz.updateQuestion(
        question.id,
        question
      );
      this.context.commit("saveQuestion", data);
      handlerSuccess({
        title: String(i18n.t("update_question")),
        text: ""
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async deleteQuestion(question: Question): Promise<void> {
    try {
      const {
        data
      }: RequestInterface<Question> = await API.quiz.deleteQuestion(
        question.id
      );
      this.context.commit("saveQuestion", data);
      handlerSuccess({
        title: String(i18n.t("delete_question")),
        text: ""
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  public get getQuestion(): Maybe<Question> {
    return this.question;
  }

  // QUESTION_OPTION --------------------------------------------------

  public questionOption: Maybe<QuestionOption> = null;

  @Mutation
  private saveQuestionOption(payload: Maybe<QuestionOption>) {
    this.questionOption = payload;
  }

  @Action
  public async addQuestionOption(
    questionOption: QuestionOption
  ): Promise<void> {
    try {
      const {
        data
      }: RequestInterface<QuestionOption> = await API.quiz.addQuestionOption(
        questionOption
      );
      this.context.commit("saveQuestionOption", data);
      handlerSuccess({
        title: String(i18n.t("add_question_option")),
        text: ""
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async updateQuestionOption(
    questionOption: Partial<QuestionOption>
  ): Promise<void> {
    try {
      const {
        data
      }: RequestInterface<QuestionOption> = await API.quiz.updateQuestionOption(
        questionOption.id,
        questionOption
      );
      this.context.commit("saveQuestionOption", data);
      handlerSuccess({
        title: String(i18n.t("update_question_option")),
        text: ""
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async deleteQuestionOption(
    questionOption: QuestionOption
  ): Promise<void> {
    try {
      const {
        data
      }: RequestInterface<QuestionOption> = await API.quiz.deleteQuestionOption(
        questionOption.id
      );
      this.context.commit("saveQuestionOption", data);
      handlerSuccess({
        title: String(i18n.t("delete_question_option")),
        text: ""
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  public get getQuestionOption(): Maybe<QuestionOption> {
    return this.questionOption;
  }

  // QUESTION_USER_ANSWER --------------------------------------------------

  public questionUserAnswer: Maybe<QuestionUserAnswer> = null;

  @Mutation
  private saveQuestionUserAnswer(payload: QuestionUserAnswer) {
    this.questionUserAnswer = payload;
  }

  @Action
  public async addQuestionUserAnswer(
    questionUserAnswer: QuestionUserAnswer
  ): Promise<void> {
    const { data } = await API.quiz.addQuestionUserAnswer(questionUserAnswer);
    this.context.commit("saveQuestionUserAnswer", data);
  }

  public get getQuestionUserAnswer() {
    return this.questionUserAnswer;
  }

  // QUESTION_WITH_ANSWERS --------------------------------------------------

  public quizWithAnswer: Maybe<QuizWithAnswersData> = null;

  @Mutation
  private saveQuizWithAnswer(payload: Maybe<QuizWithAnswersData>) {
    this.quizWithAnswer = payload;
  }

  @Action
  public async fetchQuestionWithAnswer(
      quizWithAnswersData: QuizWithAnswersData
  ): Promise<void> {
    const {
      data
    }: RequestInterface<QuizWithAnswersData> = await API.quiz.getQuestionWithAnswer(
        quizWithAnswersData.quiz_id,
        quizWithAnswersData.id
    );
    this.context.commit("saveQuizWithAnswer", data);
  }

  public get getQuizWithAnswer() {
    return this.quizWithAnswer;
  }


  // SET SCORE
  @Action
  public async SetScoreQuiz(params: any): Promise<void> {
    try {
      await API.quiz.setScoreQuiz(params.quiz_id, params);
      handlerSuccess({
        title: String(i18n.t("update_score")),
        text: ""
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
}

export { QuizModule };
