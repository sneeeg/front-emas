import { VuexModule, Module, Action, Mutation } from "vuex-module-decorators";
import API from "@/api";
import i18n from "@/i18n";
import { Maybe } from "@/helpers/types";
import { handlerError } from "@/helpers/utils/handlerError";
import { handlerSuccess } from "@/helpers/utils/handlerSuccess";

@Module
class NewsModule extends VuexModule {
  public news: Maybe<any[]> = null;
  public new: Maybe<any[]> = null;
  public testNew: Maybe<any[]> = null;

  @Mutation
  private saveNews(payload: any[]) {
    this.news = payload;
  }
  @Mutation
  private saveNew(payload: any[]) {
    this.new = payload;
  }
  @Mutation
  private testSendNew(payload: any[]) {
    this.testNew = payload;
  }

  @Action
  public async fetchNews(params: any): Promise<void> {

    try {
      const { data } = await API.news.getNews(params);
      this.context.commit("saveNews", data);
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async fetchNew(id: number): Promise<void> {

    try {
      const { data } = await API.news.getNew(id);
      this.context.commit("saveNew", data);
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async sendNews(params: any): Promise<void> {
    try {
      const { data } = await API.news.sendNews(params);
      this.context.commit("testSendNew", data);
      handlerSuccess({
        title: String(i18n.t("new_send")),
        text: "Успех"
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async makeReadNews(id: any): Promise<void> {
    try {
      await API.news.makeReadNews(id);
      handlerSuccess({
        title: String(i18n.t("new_read")),
        text: "Успех"
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async fetchRatingWithFilter(params: any): Promise<void> {

    try {
      const { data } = await API.news.getRatingWithFilter(
        params.mounth,
        params.year
      );
      this.context.commit("saveRating", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  public get getNews() {
    return this.news;
  }
  public get getNew() {
    return this.new;
  }
  public get getTestNew() {
    return this.testNew;
  }
}

export { NewsModule };
