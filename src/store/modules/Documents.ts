import { VuexModule, Module, Action, Mutation } from "vuex-module-decorators";
import API from "@/api";
import { handlerError } from "@/helpers/utils/handlerError";
import { Maybe } from "@/helpers/types";

@Module
class DocumentsModule extends VuexModule {
  public documents: Maybe<any[]> = null;
  public userDocuments: Maybe<any[]> = null;

  @Mutation
  private saveDocuments(payload: any[]) {
    this.documents = payload;
  }

  @Mutation
  private saveUserDocuments(payload: any[]) {
    this.userDocuments = payload;
  }

  @Action
  public async fetchDocuments(): Promise<void> {
    try {
      const { data } = await API.files.getUserDocuments();
      this.context.commit("saveDocuments", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async showDocuments(): Promise<void> {
    try {
      const { data } = await API.files.showUserDocuments();
      this.context.commit("saveUserDocuments", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  public get getDocuments() {
    return this.documents;
  }

  public get getUserDocuments() {
    return this.userDocuments;
  }
}

export { DocumentsModule };
