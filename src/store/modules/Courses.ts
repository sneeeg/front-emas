import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators";

import API from "@/api";
import store from "@/store";
import { Maybe, CourseInterface, DataInterface } from "@/helpers/types";
import { handlerSuccess } from "@/helpers/utils/handlerSuccess";
import { handlerError } from "@/helpers/utils/handlerError";
import i18n from "@/i18n";
@Module
class CoursesModule extends VuexModule {
  public courses: Maybe<DataInterface<CourseInterface[]>> = null;

  @Mutation
  private setCourses(payload: Maybe<DataInterface<CourseInterface[]>>) {
    this.courses = payload;
  }

  @Action
  public async fetchCourses(params = ""): Promise<void> {
    
    try {
      const { data } = await API.courses.getCourses(params);
    this.context.commit("setCourses", data);
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async fetchCoursesForUser(params = ""): Promise<void> {
    
    try {
      const { data } = await API.courses.getUserCourses(params);

    this.context.commit("setCourses", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async deleteCourse(id: number): Promise<void> {
    try {
      await API.course.deleteCourse(id);
      handlerSuccess({
        title: String(i18n.t("course_is_delete")),
        text: String(i18n.t("course_is_delete_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async deleteCourses(params: any): Promise<void> {
    try {
      await API.courses.deleteCourses(params);
      handlerSuccess({
        title: String(i18n.t("courses_is_delete")),
        text: String(i18n.t("courses_is_delete_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async duplicateCourses(params: any): Promise<void> {
    try {
      await API.courses.duplicateCourses(params);
      handlerSuccess({
        title: String(i18n.t("courses_is_clone")),
        text: String(i18n.t("courses_is_clone_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  public get getCourses(): Maybe<DataInterface<CourseInterface[]>> {
    return this.courses;
  }
}

export { CoursesModule };
