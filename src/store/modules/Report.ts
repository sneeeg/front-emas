import { VuexModule, Module, Action, Mutation } from "vuex-module-decorators";

import API from "@/api";
import { Maybe, ReportRequestType } from "@/helpers/types";
import { handlerError } from "@/helpers/utils/handlerError";
import { handlerSuccess } from "@/helpers/utils/handlerSuccess";
import { ReportType } from "@/helpers/types/reports";

@Module
class ReportModule extends VuexModule {
  public report: Maybe<ReportType[]> = null;
  public lastReportRequestParams: ReportRequestType = {
    type: "",
    params: {}
  };

  @Mutation
  private setReport(payload: Maybe<ReportType[]>) {
    this.report = payload;
  }

  @Mutation
  private setLastReportRequestParams(payload: ReportRequestType) {
    this.lastReportRequestParams = payload;
  }

  @Action
  public async fetchReport({ type, params }: ReportRequestType) {
    try {
      const { data } = await API.report.getReport(type, params);
      this.context.commit("setReport", data);
      this.context.commit("setLastReportRequestParams", { type, params });
    } catch (error) {
      this.context.commit("setReport", null);
      this.context.commit("setLastReportRequestParams", {});
      handlerError(error as any);
    }
  }
  @Action
  public async exportReport(params: any): Promise<void> {
    await API.report.exportReport(params);
    //this.context.commit("saveFile", data);
  }
  @Action
  public async setCommentToDebt(params: any): Promise<void> {
    //await API.report.setCommentToDebt(params.id, params);
    //this.context.commit("saveFile", data);
    try {
      await API.report.setCommentToDebt(params.id, params);
      handlerSuccess({
        title: "Комментарий успешно отправлен",
        text: "Комментарий успешно отправлен"
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  public get getReport(): Maybe<ReportType[]> {
    return this.report;
  }

  public get getLastReportRequestParams(): ReportRequestType {
    return this.lastReportRequestParams;
  }
}

export { ReportModule };
