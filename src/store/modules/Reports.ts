import { Module, VuexModule } from "vuex-module-decorators";
import { Maybe, ReportLinkType, ReportTypes } from "@/helpers/types";

@Module
class ReportsModule extends VuexModule {
  public reports: Maybe<ReportLinkType[]> = [
    {
      title: "report_tests",
      children: [
        {
          id: ReportTypes.QUIZ_SCORE_RESULTS,
          iconName: "chart-pie",
          name: "results",
          description: "users_points"
        },
        {
          id: ReportTypes.QUIZ_ANSWER_BREAKDOWN,
          iconName: "chart-pie",
          name: "report_analyz",
          description: "answers_given"
        }
        // {
        //     id: ReportTypes.QUIZ_ATTEMPT_DETAIL,
        //     iconName: "chart-pie",
        //     name: "Детали попыток",
        //     description: "Как ответил пользователь?",
        //     disabled: true
        // },
        // {
        //     id: ReportTypes.PROGRESS_ACHIEVEMENT,
        //     iconName: "chart-pie",
        //     name: "Достижения учащихся",
        //     description:
        //         "Обзор для группы тестов, диалогов, заданий Обзор для группы тестов, диалогов, заданий Обзор для группы тестов, диалогов, заданий",
        //     disabled: true
        // },
        // {
        //     id: ReportTypes.ASSIGNMENT_RESULTS,
        //     iconName: "chart-pie",
        //     name: "Результаты заданий",
        //     description: "Как пользователи проходят задание?",
        //     disabled: true
        // }
      ]
    },
    // {
    //     title: "Отчеты по материалам",
    //     children: [
    //         {
    //             id: ReportTypes.CONTENT_ACTIVITY,
    //             iconName: "chart-pie",
    //             name: "Действия",
    //             description:
    //                 "Какие над данным материалом совершены действия?",
    //             disabled: true
    //         },
    //         {
    //             id: ReportTypes.PRESENTATION_TRAFFIC,
    //             iconName: "chart-pie",
    //             name: "Трафик",
    //             description: "Как часто просматривали материал?",
    //             disabled: true
    //         },
    //         {
    //             id: ReportTypes.PRESENTATION_PROGRESS,
    //             iconName: "chart-pie",
    //             name: "Прогресс",
    //             description: "Сколько слайдов/страниц просмотрено?",
    //             disabled: true
    //         },
    //         {
    //             id: ReportTypes.PRESENTATION_POPULAR,
    //             iconName: "chart-pie",
    //             name: "Популярный материал",
    //             description: "Какой материал просматривали чаще всего?",
    //             disabled: true
    //         },
    //         {
    //             id: ReportTypes.PRESENTATION_SLIDE_VIEWS,
    //             iconName: "chart-pie",
    //             name: "Просмотры слайда или страницы",
    //             description: "Сколько раз просматривали слайд/страницу?",
    //             disabled: true
    //         },
    //         {
    //             id: ReportTypes.PERFORMANCE_SUMMARY,
    //             iconName: "chart-pie",
    //             name: "Сводка достижений",
    //             description: "Чего достигли учащиеся?",
    //             disabled: true
    //         }
    //     ]
    // },
    {
      title: "report_programs",
      children: [
        {
          id: ReportTypes.LEARNING_PATH_SUMMARY,
          iconName: "chart-pie",
          name: "summary_programs",
          description: "complete_success"
        }
        // {
        //     id: ReportTypes.LEARNING_PATH_DETAILS,
        //     iconName: "chart-pie",
        //     name: "Детали по программе обучения",
        //     description:
        //         "Как пользователь проходит программу обучения?",
        //     disabled: true
        // }
      ]
    },
    {
      title: "report_users",
      children: [
        // {
        //     id: ReportTypes.GROUP_ACTIVITY,
        //     iconName: "chart-pie",
        //     name: "Действия группы",
        //     description: "Какой материал просмотрен группой?",
        //     disabled: true
        // },
        {
          id: ReportTypes.USER_ACTIVITY,
          iconName: "chart-pie",
          name: "user_actions",
          description: "material_view_user"
        },
        {
          id: ReportTypes.TRAFFIC_LIGHTS,
          iconName: "chart-pie",
          name: "traffic_light",
          description: "material_view_user"
        },
        {
          id: ReportTypes.LOOKED_NEWS,
          iconName: "chart-pie",
          name: "looked_news",
          description: "looked_news_desc"
        },
        {
          id: ReportTypes.SUBMITTED_PROJECT,
          iconName: "chart-pie",
          name: "submitted_project",
          description: "submitted_project_desc"
        }
        // {
        //     id: ReportTypes.ACTIVE_GROUPS,
        //     iconName: "chart-pie",
        //     name: "Активные группы",
        //     description: "Какие группы самые активные?",
        //     disabled: true
        // },
        // {
        //     id: ReportTypes.ACTIVE_USERS,
        //     iconName: "chart-pie",
        //     name: "Активные пользователи",
        //     description: "Какие пользователи самые активные?",
        //     disabled: true
        // },
        // {
        //     id: ReportTypes.GUESTBOOK_RESPONSES,
        //     iconName: "chart-pie",
        //     name: "Гостевая книга",
        //     description: "Кто смотрел открытый материал?",
        //     disabled: true
        // }
      ]
    }
    // {
    //     title: "Отчеты по продажам",
    //     children: [
    //         {
    //             id: ReportTypes.SALES_HISTORY,
    //             iconName: "chart-pie",
    //             name: "История продаж",
    //             description: "Сколько материалов было продано?",
    //             disabled: true
    //         },
    //         {
    //             id: ReportTypes.SALES_SUMMARY,
    //             iconName: "chart-pie",
    //             name: "Сводка продаж",
    //             description: "Сколько составляет прибыль?",
    //             disabled: true
    //         }
    //     ]
    // },
    // {
    //     title: "Отчеты по мероприятиям",
    //     children: [
    //         {
    //             id: ReportTypes.EVENTS_ATTENDANCE,
    //             iconName: "chart-pie",
    //             name: "Посещение мероприятий",
    //             description: "Узнайте, кто из учащихся посетил мероприятие",
    //             disabled: true
    //         },
    //         {
    //             id: ReportTypes.ACCOMPLISHED_EVENTS,
    //             iconName: "chart-pie",
    //             name: "Проведенные мероприятия",
    //             description:
    //                 "Сколько мероприятий проведено за выбранный период",
    //             disabled: true
    //         }
    //     ]
    // }
  ];

  public get getReports(): Maybe<ReportLinkType[]> {
    return this.reports;
  }
}

export { ReportsModule };
