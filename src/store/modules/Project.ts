import { VuexModule, Module, Action, Mutation } from "vuex-module-decorators";
import API from "@/api";
import { handlerError } from "@/helpers/utils/handlerError";
import { handlerSuccess } from "@/helpers/utils/handlerSuccess";
import { Maybe } from "@/helpers/types";
import i18n from "@/i18n";
@Module
class ProjectModule extends VuexModule {
  public projects: Maybe<any[]> = null;
  public project: Maybe<any[]> = null;

  @Mutation
  private saveProjects(payload: any[]) {
    this.projects = payload;
  }
  @Mutation
  private saveProject(payload: any[]) {
    this.project = payload;
  }

  @Action
  public async fetchProjects(params = ""): Promise<void> {
    try {
      const { data } = await API.project.getProjects(params);
      this.context.commit("saveProjects", data);
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async fetchProject(id: number): Promise<void> {
    try {
      const { data } = await API.project.getProject(id);
      this.context.commit("saveProject", data);
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async editProject(params: any): Promise<void> {
    try {
      await API.project.editProject(params);
      handlerSuccess({
        title: String(i18n.t("project_edit")),
        text: String(i18n.t("project_edit_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async sendProjectDocumentList(params: any): Promise<void> {
    try {
      const data = await API.project.sendProjectDocumentList(params);
      console.log("sendProjectDocumentList", data);
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async approveProject(params: any): Promise<void> {
    try {
      await API.project.approveProject(params);
      handlerSuccess({
        title: String(i18n.t("project_edit")),
        text: String(i18n.t("project_edit_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  public get getProjects() {
    return this.projects;
  }
  public get getProject() {
    return this.project;
  }
}

export { ProjectModule };
