import { VuexModule, Module, Action, Mutation } from "vuex-module-decorators";
import API from "@/api";
import { handlerError } from "@/helpers/utils/handlerError";
import { handlerSuccess } from "@/helpers/utils/handlerSuccess";
import { Maybe } from "@/helpers/types";
import i18n from "@/i18n";
@Module
class PriceModule extends VuexModule {
  public prices: Maybe<any[]> = null;
  public price: Maybe<any[]> = null;

  @Mutation
  private savePrices(payload: any[]) {
    this.prices = payload;
  }
  @Mutation
  private savePrice(payload: any) {
    this.price = payload;
  }

  @Action
  public async fetchPrice(): Promise<void> {
    try {
      const { data } = await API.price.getPrices();
      this.context.commit("savePrices", data);
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async fetchPriceById(id: number): Promise<void> {
    try {
      const { data } = await API.price.getPriceById(id);
      this.context.commit("savePrice", data);
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async updatePrice(params: any): Promise<void> {
    try {
      const { data } = await API.price.updatePrice(params);
      this.context.commit("savePrice", data);
      handlerSuccess({
        title: String(i18n.t("save_price")),
        text: "Успех"
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async deletePriceById(id: number): Promise<void> {
    try {
      const { data } = await API.price.deletePriceById(id);
      //this.context.commit("savePrice", data);
      handlerSuccess({
        title: String(i18n.t("delete_price")),
        text: "Успех"
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async addPrice(params: any): Promise<void> {
    try {
      const { data } = await API.price.addPrice(params);
      //this.context.commit("savePrices", data);
      handlerSuccess({
        title: String(i18n.t("add_price_success")),
        text: "Успех"
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  public get getPrices() {
    return this.prices;
  }
  public get getPrice() {
    return this.price;
  }
}

export { PriceModule };
