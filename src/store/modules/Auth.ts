import axios from "axios";
import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators";

import API from "@/api";
import { UserInterface } from "@/helpers/types";
import { handlerError } from "@/helpers/utils/handlerError";

const token = localStorage.getItem("user-token") || "";

@Module
class AuthModule extends VuexModule {
  public token: string = token;

  @Mutation
  private setToken(data: { token: string; remember?: boolean }) {
    this.token = data.token;
    localStorage.setItem("user-token", data.token);
  }

  @Mutation
  private deleteToken() {
    localStorage.removeItem("user-token");
    this.token = "";

    window.location.reload();
  }

  @Action
  public async login(user: UserInterface): Promise<void> {
    try {
      const data = await API.auth.login(user);
      axios.defaults.headers.Authorization = `${data.token_type} ${data.access_token}`;

      this.context.commit("setToken", {
        token: data.access_token,
        remember: user.remember
      });
      await this.context.dispatch("fetchProfile");
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async logout(): Promise<void> {
    axios.defaults.headers.Authorization = ``;
    this.context.commit("deleteToken");
  }

  public get isAuthenticated(): boolean {
    return !!this.token;
  }
}

export { AuthModule };
