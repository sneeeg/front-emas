import { VuexModule, Module, Action, Mutation } from "vuex-module-decorators";
import API from "@/api";
import { handlerSuccess } from "@/helpers/utils/handlerSuccess";
import { handlerError } from "@/helpers/utils/handlerError";
import { Maybe } from "@/helpers/types";
import i18n from "@/i18n";
@Module
class TemplatesModule extends VuexModule {
  public templates: Maybe<any[]> = null;
  public template: any = null;

  @Mutation
  private saveTemplates(payload: any[]) {
    this.templates = payload;
  }
  @Mutation
  private saveTemplate(payload: any[]) {
    this.template = payload;
  }

  @Action
  public async fetchTemplates(params = ""): Promise<void> {
    const { data } = await API.files.getTemplates(params);
    this.context.commit("saveTemplates", data);
  }
  @Action
  public async deleteDocs(params: any): Promise<void> {
    try {
      await API.files.deleteDocs(params);
      handlerSuccess({
        title: String(i18n.t("docs_is_delete")),
        text: String(i18n.t("docs_is_delete_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async getTemplate(id: number): Promise<void> {
    const { data } = await API.files.getTemplateById(id);
    this.context.commit("saveTemplate", data);
  }

  public get getTemplates() {
    return this.templates;
  }
  public get getCurrentTemplate() {
    return this.template;
  }
}

export { TemplatesModule };
