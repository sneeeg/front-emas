import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators";

import API from "@/api";
import { Maybe, MediaInterface } from "@/helpers/types";
import { handlerError } from "@/helpers/utils/handlerError";
import { handlerSuccess } from "@/helpers/utils/handlerSuccess";
import router from "@/router";
import i18n from "@/i18n";
@Module
class FileManagerModule extends VuexModule {
  public files: Maybe<MediaInterface[]> = null;
  public filesToMove: Maybe<MediaInterface[]> = null;
  public file: Maybe<MediaInterface> = null;

  @Mutation
  private setFiles(payload: MediaInterface[]) {
    this.files = payload;
  }
  @Mutation
  private setFilesForMove(payload: MediaInterface[]) {
    this.filesToMove = payload;
  }
  @Mutation
  private saveFile(payload: MediaInterface) {
    this.file = payload;
  }

  @Action
  public async fetchFiles(path = ""): Promise<void> {
    try {
      let data: any[];
      if (path === "/") {
        data = await API.files.getFiles(path);
      } else {
        data = await API.files.getFiles("/" + path + "/");
      }
      this.context.commit("setFiles", data);
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async fetchFilesForMove(path = ""): Promise<void> {
    try {
      let data: any[];
      if (path === "/") {
        data = await API.files.getFiles(path);
      } else {
        data = await API.files.getFiles("/" + path + "/");
      }
      this.context.commit("setFilesForMove", data);
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async getFileById(id: number): Promise<void> {
    try {
      const { data } = await API.files.getFileById(id);
      this.context.commit("saveFile", data);
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async deleteFileById(id: number): Promise<void> {
    try {
      await API.files.deleteFileById(id);
      handlerSuccess({
        title: String(i18n.t("material_is_delete")),
        text: String(i18n.t("material_is_delete_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async uploadUserDocuments(params: any): Promise<void> {
    try {
      await API.files.uploadUserDocuments(params);
      handlerSuccess({
        title: String(i18n.t("file_is_upload")),
        text: String(i18n.t("file_is_upload_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async readMediaByAdmin(params: {
    id_media: number;
    id_user: number;
  }): Promise<void> {
    try {
      await API.files.readMediaByAdmin(params.id_media, params.id_user);
      handlerSuccess({
        title: String(i18n.t("media_is_read")),
        text: ""
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  @Action
  public async moveFile(params: any): Promise<void> {
    try {
      await API.files.moveFile(params);
      handlerSuccess({
        title: String(i18n.t("file_is_move")),
        text: ""
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async copyFile(params: any): Promise<void> {
    try {
      await API.files.copyFile(params);
      handlerSuccess({
        title: String(i18n.t("file_is_copy")),
        text: ""
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async createFolder(path: string): Promise<void> {
    try {
      await API.files.createFolder(path);

      const currentPath = router.currentRoute?.params?.path || "/";

      if (currentPath) {
        await this.context.dispatch("fetchFiles", currentPath);
      }
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async deleteFolder(path: string): Promise<void> {
    try {
      await API.files.deleteFolder(path);

      const currentPath = router.currentRoute?.params?.path || "/";

      if (currentPath) {
        await this.context.dispatch("fetchFiles", currentPath);
      }
    } catch (error) {
      handlerError(error as any);
    }
  }

  public get getFiles(): Maybe<MediaInterface[]> {
    return this.files;
  }
  public get getFilesForMove(): Maybe<MediaInterface[]> {
    return this.filesToMove;
  }
  public get getFile(): Maybe<MediaInterface> {
    return this.file;
  }
}

export { FileManagerModule };
