import { VuexModule, Module, Action, Mutation } from "vuex-module-decorators";
import API from "@/api";
import i18n from "@/i18n";
import { Maybe } from "@/helpers/types";
import { handlerError } from "@/helpers/utils/handlerError";
import { handlerSuccess } from "@/helpers/utils/handlerSuccess";
@Module
class MediaModule extends VuexModule {
  @Action
  public async SubmitProject({
    id,
    file
  }: {
    id: number;
    file: File;
  }): Promise<void> {
    const params = {
      file: file
    };
    try {
      await API.media.SubmitProject(id, params);
      handlerSuccess({
        title: String(i18n.t("submit_project")),
        text: ""
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async ScoreAndTrainerForSubmitProject(params: any): Promise<void> {
    try {
      await API.media.ScoreAndTrainerForSubmitProject(params.user_id, params.media_id, params.submit_id, params);
      handlerSuccess({
        title: String(i18n.t("score_trainer_submit_project")),
        text: ""
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async SendFilesFromSubmittedProject(params: any) {
    await API.media.SendFilesFromSubmittedProject();
  }
  @Action
  public async DownloadSubmitProject(params: any): Promise<void> {
    try {
      await API.media.DownloadSubmitProject(params.media_id, params.submit_id);
      handlerSuccess({
        title: String(i18n.t("download_success")),
        text: "Успех"
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
}

export { MediaModule };
