import { VuexModule, Module, Action, Mutation } from "vuex-module-decorators";
import API from "@/api";
import { handlerError } from "@/helpers/utils/handlerError";
import { Maybe } from "@/helpers/types";
import i18n from "@/i18n";
@Module
class RatingModule extends VuexModule {
  public rating: Maybe<any[]> = null;

  @Mutation
  private saveRating(payload: any[]) {
    this.rating = payload;
  }

  @Action
  public async fetchRating(): Promise<void> {

    try {
      const { data } = await API.rating.getRating();
      this.context.commit("saveRating", data);
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async fetchRatingWithFilter(params: any): Promise<void> {

    try {
      const { data } = await API.rating.getRatingWithFilter(
        params.mounth,
        params.year
      );
      this.context.commit("saveRating", data);
    } catch (error) {
      handlerError(error as any);
    }
  }

  public get getRating() {
    return this.rating;
  }
}

export { RatingModule };
