import { VuexModule, Module, Action, Mutation } from "vuex-module-decorators";
import API from "@/api";
import i18n from "@/i18n";
import { GroupInterface, Maybe, UserInterface } from "@/helpers/types";
import { handlerSuccess } from "@/helpers/utils/handlerSuccess";
import { handlerError } from "@/helpers/utils/handlerError";
@Module
class GroupsModule extends VuexModule {
  public groups: Maybe<GroupInterface[]> = null;
  public group: Maybe<GroupInterface> = null;

  @Mutation
  private saveGroups(payload: GroupInterface[]) {
    this.groups = payload;
  }
  @Mutation
  private saveGroup(payload: GroupInterface) {
    this.group = payload;
  }

  @Action
  public async fetchGroups(params = ""): Promise<void> {

    try {
      const { data } = await API.group.getGroups(params);
      this.context.commit("saveGroups", data);
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async getGroup(id: number): Promise<void> {

    try {
      const { data } = await API.group.getGroup(id);
      this.context.commit("saveGroup", data);
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async addGroup(params: {
    name: string;
    description: string;
  }): Promise<void> {
    try {
      await API.group.addGroup(params);
      handlerSuccess({
        title: String(i18n.t("group_is_add")),
        text: String(i18n.t("group_is_add_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async deleteGroup(id: number): Promise<void> {
    try {
      await API.group.deleteGroup(id);
      handlerSuccess({
        title: String(i18n.t("group_is_delete")),
        text: String(i18n.t("group_is_delete_desc"))
      });
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async addGroupToUser({
    user,
    id
  }: {
    user: UserInterface;
    id: number;
  }): Promise<void> {

    try {
      const params = {
        users: [user.id]
      };
      const { data } = await API.group.setUsersToGroup(id, params);
      this.context.commit("saveGroup", data);
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async deleteGroupToUser({
    user,
    id
  }: {
    user: UserInterface;
    id: number;
  }): Promise<void> {

    try {
      const params = {
        users: [user.id]
      };
      const { data } = await API.group.deleteUsersToGroup(id, params);
      this.context.commit("saveGroup", data);
    } catch (error) {
      handlerError(error as any);
    }
  }
  @Action
  public async editGroup(params: {
    name: string;
    description: string;
    id: number;
  }): Promise<void> {
    try {
      await API.group.editGroup(params);
      handlerSuccess({
        title: String(i18n.t("group_is_edit")),
        text: ""
      });
    } catch (error) {
      handlerError(error as any);
    }
  }

  public get getGroups() {
    return this.groups;
  }
  public get getterGroup() {
    return this.group;
  }
}

export { GroupsModule };
