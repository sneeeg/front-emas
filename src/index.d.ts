declare module "*.vue" {
  import Vue from "vue";
  export default Vue;
}

declare module "*.svg";
declare module "*.png";

declare module "*.json" {
  const value: any;
  export default value;
}

declare module "@/api" {
  import API from "@/api";
  export default API;
}

declare module "@/router" {
  import * as router from "@/router";
  export default router;
}
declare module "@/helpers" {
  import * as helpers from "@/helpers";
  export default helpers;
}

declare module "v-click-outside" {
  import vClickOutside from "v-click-outside";
  export default vClickOutside;
}

declare module "vue-extend-layout" {
  import VueExtendLayouts from "vue-extend-layout";
  export default VueExtendLayouts;
}

declare module "vue-good-table" {
  import { VueGoodTable } from "vue-good-table";
  export { VueGoodTable };
}
declare module "vue-badger-accordion" {
  import { BadgerAccordion, BadgerAccordionItem } from 'vue-badger-accordion'
  export { BadgerAccordion, BadgerAccordionItem };
}
declare module "vue-awesome/components/Icon" {
  import Icon from "vue-awesome/components/Icon";
  export default Icon;
}

declare module "vue2-datepicker" {
  import DatePicker from "vue2-datepicker";
  import "vue2-datepicker/index.css";
  export default DatePicker;
}

declare module "vue-multilanguage";
declare const Buffer;
