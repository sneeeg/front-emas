export enum Locales {
  RU = "ru",
  EN = "en"
}

export const LOCALES = [
  { value: Locales.RU, caption: "Русский" },
  { value: Locales.EN, caption: "English" }
];
