import Vue from "vue";
import VueI18n from "vue-i18n";

import { Locales } from "./locales";

import en from "./en.json";
import ru from "./ru.json";

export const messages = {
  [Locales.EN]: en,
  [Locales.RU]: ru
};
Vue.use(VueI18n);
const i18n = new VueI18n({
  messages,
  locale: localStorage.getItem("language") || "ru",
  fallbackLocale: Locales.RU
});

export const defaultLocale = Locales.RU;
export default i18n;
