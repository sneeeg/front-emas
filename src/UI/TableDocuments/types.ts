interface PaginationOptionsInterface {
  enabled: boolean;
  mode: string;
  position: string;
  currentPage?: number;
  currentPerPage?: number;
  perPageDropdown?: number[];
  dropdownAllowAll?: boolean;
  nextLabel: string;
  prevLabel: string;
  rowsPerPageLabel: string;
  ofLabel: string;
  pageLabel: string;
  allLabel: string;
}

export { PaginationOptionsInterface };
