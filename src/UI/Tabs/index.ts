import Tabs from "./Tabs.vue";
import TabContent from "./TabContent.vue";
import TabHeader from "./TabHeader.vue";

export { Tabs, TabContent, TabHeader };
