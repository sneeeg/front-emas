import TextField from "./TextField.vue";
import TextFieldAuth from "./TextFieldAuth.vue";
import TextFieldValidation from "./TextFieldValidation.vue";
import Password from "./Password.vue";
import PasswordAuth from "./PasswordAuth.vue";
import PasswordValidation from "./PasswordValidation.vue";
import Autocomplete from "./Autocomplete.vue";
import FileUploader from "./FileUploader.vue";
import Button from "./Button.vue";
import Loader from "./Loader.vue";
import Title from "./Title.vue";
import Subtitle from "./Subtitle.vue";
import Link from "./Link.vue";
import Modal from "./Modal.vue";
import ModalRequired from "./ModalRequired.vue";
import FormItem from "./Form/FormItem.vue";
import Picture from "./Picture.vue";
import ProgressBar from "./ProgressBar.vue";
import Separator from "./Separator.vue";
import Select from "./Select.vue";
import SelectSearch from "./SelectSearch.vue";
import Datepicker from "@/UI/Datepicker.vue";
import DatepickerMain from "@/UI/DatepickerMain.vue";
import Checkbox from "./Checkbox.vue";
import CheckboxOne from "./CheckboxOne.vue";
import Radio from "./Radio.vue";
import DraggableList from "./DraggableList.vue";
import DraggableListItem from "./DraggableListItem.vue";
import {
  Page,
  PageHeader,
  PageContent,
  PageEditHeader,
  PageContentUploader,
  PageEditTabs
} from "./Page";
import { Tabs, TabContent, TabHeader } from "./Tabs";
import { Table } from "./Table";
import { TableQuizzes } from "./TableQuizzes";
import { TableUsers } from "./TableUsers";
import { TableRating } from "./TableRating";
import { TableFiles } from "./TableFiles";
import { TableCourses } from "./TableCourses";
import { TableScore } from "./TableScore";
import { TableGroup } from "./TableGroup";
import { TableDocuments } from "./TableDocuments";
import { TableOneSelect } from "./TableOneSelect";

export {
  TextField,
  TextFieldValidation,
  Password,
  PasswordValidation,
  Autocomplete,
  FileUploader,
  Button,
  Link,
  Loader,
  Tabs,
  Modal,
  ModalRequired,
  Title,
  Subtitle,
  FormItem,
  TabHeader,
  TabContent,
  Table,
  TableQuizzes,
  Picture,
  ProgressBar,
  Separator,
  Select,
  SelectSearch,
  Datepicker,
  Checkbox,
  CheckboxOne,
  Radio,
  DraggableList,
  DraggableListItem,
  Page,
  PageHeader,
  PageContent,
  PageEditHeader,
  PageContentUploader,
  PageEditTabs,
  TableUsers,
  TableRating,
  TableFiles,
  TableCourses,
  TableScore,
  TableGroup,
  TableDocuments,
  DatepickerMain,
  TableOneSelect,
  TextFieldAuth,
  PasswordAuth
};
