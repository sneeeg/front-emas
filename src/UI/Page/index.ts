import Page from "./Page.vue";
import PageHeader from "./PageHeader.vue";
import PageContent from "./PageContent.vue";
import PageContentUploader from "./PageContentUploader.vue";
import PageEditHeader from "./PageEditHeader.vue";
import PageEditTabs from "./PageEditTabs.vue";

export {
  Page,
  PageHeader,
  PageContent,
  PageContentUploader,
  PageEditHeader,
  PageEditTabs
};
