import { modules } from "./modules";
import axios, { Method } from "axios";
axios.defaults.baseURL = process.env.VUE_APP_API_URL;

const token = localStorage.getItem("user-token") || "";

if (token) {
  axios.defaults.headers.Authorization = `Bearer ${token}`;
}

const request = async (
  url: string,
  method: Method,
  data?: unknown,
  headers?: any
): Promise<any> => {
  return await new Promise((resolve, reject) =>
    axios({
      method,
      data,
      url,
      headers
    })
      .then(response => resolve(response.data))
      .catch(error => reject(error?.response?.data))
  );
};

export default {
  ...modules,
  request
};
