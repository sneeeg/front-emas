import API from "@/api";
import { ChapterInterface, CourseInterface } from "@/helpers/types";

export default {
  getCourse: (id: number) => API.request(`/admin/program/${id}`, "get"),
  getUserCourse: (id: number) => API.request(`/api/program/${id}`, "get"),
  getUserCourseAdmin: (id: number) =>
    API.request(`/api/program?user_id=${id}`, "get"),
  createCourse: (params: CourseInterface) =>
    API.request(`/admin/program`, "post", params),
  editCourse: (id: number, params: CourseInterface) =>
    API.request(`/admin/program/${id}`, "put", params),
  deleteCourse: (id: number) => API.request(`/admin/program/${id}`, "delete"),
  cloneCourse: (id: number) =>
    API.request(`/admin/program/${id}/clone`, "post"),
  createChapter: (params: CourseInterface) =>
    API.request(`/admin/programChapter`, "post", params),
  editChapter: (id: number, params: ChapterInterface) =>
    API.request(`/admin/programChapter/${id}`, "put", params),
  setUsers: (id: number, params: { users: number[] }) =>
    API.request(`/admin/program/${id}/setUser`, "post", params),
  deleteUsers: (id: number, params: { users: number[] }) =>
    API.request(`/admin/program/${id}/destroyUser`, "delete", params),
  setGroups: (id: number, params: { groups: number[] }) =>
    API.request(`/admin/program/${id}/setGroup`, "post", params),
  deleteGroups: (id: number, params: { groups: number[] }) =>
    API.request(`/admin/program/${id}/destroyGroup`, "delete", params),
  deleteChapter: (id: number) =>
    API.request(`/admin/programChapter/${id}`, "delete"),
  attachMediaToChapter: (id: number, params: any) =>
    API.request(`/admin/programchapter/${id}/attachMedia`, "post", params),
  sortChapterMedia: (id: number, params: any) =>
    API.request(`/admin/programchapter/${id}/sortMedia`, "post", params),
  sortChapter: (id: number, params: any) =>
    API.request(`/admin/program/${id}/sortChapter`, "post", params),
  sortChapterQuizzes: (id: number, params: any) =>
    API.request(`/admin/programchapter/${id}/sortQuiz`, "post", params),
  attachQuizzesToChapter: (id: number, params: any) =>
    API.request(`/admin/programchapter/${id}/attachQuiz`, "post", params),
  deleteMediaChapter: (id: number, params: any) =>
    API.request(`/admin/programchapter/${id}/detachMedia`, "post", params),
  deleteQuizzChapter: (id: number, params: any) =>
    API.request(`/admin/programchapter/${id}/detachQuiz`, "post", params)
};
