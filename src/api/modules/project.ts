import API from "@/api";
import { DataInterface } from "@/helpers/types";
import { Quiz } from "@/helpers/types/quiz";
import { RequestInterface } from "@/helpers/types/common";

export default {
  getProjects: (
    params: DataInterface<Quiz[]>
  ): Promise<RequestInterface<DataInterface<Quiz[]>>> => {
    return API.request(`/admin/submittedProject?${params}`, "get");
  },
  getProject: (id: number) =>
    API.request(`/admin/submittedProject/${id}`, "get"),
  editProject: (params: any) =>
    API.request(`/admin/submittedProject/${params.id}`, "put", params),
  approveProject: (params: any) =>
    API.request(`/admin/submittedProject/${params.id}/approve`, "put", params),

  sendProjectDocumentList: (params: any) => {
    const fd = new FormData();
    fd.append("_method", "put");
    params.files.forEach((item: any, index: any) => {
      fd.append("file" + ++index, item);
    });
    fd.append("score", params.score);
    fd.append("message", params.message);
    return API.request(`/admin/submittedProject/${params.id}`, "post", fd, {
      headers: {
        "content-type": "multipart/form-data"
      }
    });
  }
};
