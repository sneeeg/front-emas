import API from "@/api";
import { CourseInterface, DataInterface } from "@/helpers/types";

export default {
  getGroups: (params = "") => API.request(`/admin/group?${params}`, "get"),
  getGroup: (id: number) => API.request(`/admin/group/${id}`, "get"),
  addGroup: (params: { name: string; description: string }) =>
    API.request(`/admin/group`, "post", params),
  deleteGroup: (id: number) => API.request(`/admin/group/${id}`, "delete"),
  editGroup: (params: { id: number; name: string; description: string }) =>
    API.request(`/admin/group/${params.id}`, "put", params),
  setUsersToGroup: (id: number, params: { users: number[] }) =>
    API.request(`/admin/group/${id}/setUser`, "post", params),
  deleteUsersToGroup: (id: number, params: { users: number[] }) =>
    API.request(`/admin/group/${id}/detachUser`, "post", params)
};
