import API from "@/api";
import { DataInterface, RequestInterface } from "@/helpers/types/common";
import {
  Quiz,
  Question,
  QuestionOption,
  QuestionUserAnswer,
  QuizAttemptData,
  QuizWithAnswersData
} from "@/helpers/types/quiz";

export default {
  /** Получить все тесты */
  getAllQuizzes: (
    params: DataInterface<Quiz[]>
  ): Promise<RequestInterface<DataInterface<Quiz[]>>> => {
    return API.request(`/admin/quiz?${params}`, "get");
  },

  /** Добавить тест */
  addQuiz: (quiz: Quiz): Promise<RequestInterface<Quiz>> => {
    return API.request("/admin/quiz", "post", quiz);
  },

  /** Получить тест с вопросами и ответами на них */
  getQuiz: (id: Quiz["id"]): Promise<RequestInterface<Quiz>> => {
    return API.request(`/admin/quiz/${id}`, "get");
  },

  getQuizAttemptData: (
    id: Quiz["id"]
  ): Promise<RequestInterface<QuizAttemptData>> => {
    return API.request(`/api/quiz/${id}`, "get");
  },
  getQuizNewAttemptData: (
    id: Quiz["id"]
  ): Promise<RequestInterface<QuizAttemptData>> => {
    return API.request(`/api/quiz/${id}?is_new=true`, "get");
  },

  getQuestionWithAnswer: (
    id: QuizWithAnswersData["quiz_id"],
    id_quizAttemp: QuizWithAnswersData["id"]
  ): Promise<RequestInterface<QuizWithAnswersData>> => {
    return API.request(
      `/api/quiz/${id}/questionWithAnswer?id_quizAttemp=${id_quizAttemp}`,
      "get"
    );
  },

  getQuizQuestion: (
    id: QuizAttemptData["quiz_id"],
    id_quizAttemp: QuizAttemptData["id"]
  ): Promise<RequestInterface<Question>> => {
    return API.request(
      `/api/quiz/${id}/question?id_quizAttemp=${id_quizAttemp}`
    );
  },

  /** Обновить тест */
  updateQuiz: (
    id: Quiz["id"],
    quiz: Partial<Quiz>
  ): Promise<RequestInterface<Quiz>> => {
    return API.request(`/admin/quiz/${id}`, "post", quiz);
  },

  /** Удалить тест */
  deleteQuiz: (id: Quiz["id"]): Promise<RequestInterface<Quiz>> => {
    return API.request(`/admin/quiz/${id}`, "delete");
  },

  /** Получить все вопросы теста
   * @param {object} params - Параметры запроса
   * @param {number} params.quiz_id - ID теста
   */
  getAllQuestions: (params = ""): Promise<RequestInterface<Question>> => {
    return API.request(`/admin/question?${params}`, "get");
  },

  /** Добавить вопрос к тесту */
  addQuestion: (question: Question): Promise<RequestInterface<Question>> => {
    return API.request("/admin/question", "post", question);
  },

  /** Обновить вопрос */
  updateQuestion: (
    id: Question["id"],
    question: Partial<Question>
  ): Promise<RequestInterface<Question>> => {
    return API.request(`/admin/question/${id}`, "post", question);
  },

  /** Удалить вопрос */
  deleteQuestion: (id: Question["id"]): Promise<RequestInterface<Question>> => {
    return API.request(`/admin/question/${id}`, "delete");
  },

  /** Получить вопрос */
  getQuestion: (id: Question["id"]): Promise<RequestInterface<Question>> => {
    return API.request(`/admin/question/${id}`, "get");
  },

  /** Добавить вариант ответа к вопросу */
  addQuestionOption: (
    questionOption: QuestionOption
  ): Promise<RequestInterface<QuestionOption>> => {
    return API.request("/admin/QuestionOption", "post", questionOption);
  },

  /** Обновить вариант ответа */
  updateQuestionOption: (
    id: QuestionOption["id"],
    questionOption: Partial<QuestionOption>
  ): Promise<RequestInterface<QuestionOption>> => {
    return API.request(`/admin/QuestionOption/${id}`, "put", questionOption);
  },

  /** Удалить вариант ответа */
  deleteQuestionOption: (
    id: QuestionOption["id"]
  ): Promise<RequestInterface<QuestionOption>> => {
    return API.request(`/admin/QuestionOption/${id}`, "delete");
  },
  /** Получить вариант ответа */
  getQuestionOption: (
    id: QuestionOption["id"]
  ): Promise<RequestInterface<QuestionOption>> => {
    return API.request(`/admin/QuestionOption/${id}`, "get");
  },

  /** Отправка пользовательского ответа на вопрос */
  addQuestionUserAnswer: (
    questionUserAnswer: QuestionUserAnswer
  ): Promise<RequestInterface<QuestionUserAnswer>> => {
    return API.request("/api/QuestionUserAnswer", "post", questionUserAnswer);
  },

  deleteQuizzes: (params: any) =>
    API.request(`/admin/delete/quiz`, "delete", params),

  setScoreQuiz: (quiz_id: number, params: { user_id: number, score: number }) => {
    return API.request(`admin/quiz/${quiz_id}/setScore`, "put", params);
  },
};
