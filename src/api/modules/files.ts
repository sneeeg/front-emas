import API from "@/api";
import { DataInterface } from "@/helpers/types";

export default {
  getFiles: (path: string) =>
    API.request(`/admin/file/getFolder?path=${path}`, "get"),
  getTemplates: (params: DataInterface<any[]>) =>
    API.request(`/admin/document?${params}`, "get"),
  getTemplateById: (id: number) => API.request(`/admin/document/${id}`, "get"),
  deleteDocs: (params: any) => API.request(`/admin/document`, "delete", params),
  getUserDocuments: () => API.request(`/api/documents`, "get"),
  showUserDocuments: () => API.request(`/api/document/showUploadDocument`, "get"),
  uploadUserDocuments: (params: any) =>
    API.request(`/api/document`, "post", params),
  getFileById: (id: number) => API.request(`/admin/media/${id}`, "get"),
  readMediaByAdmin: (id_media: number, id_user: number) =>
    API.request(`/admin/user/${id_user}/media/${id_media}/setViewMedia`, "put"),
  deleteFileById: (id: number) => API.request(`/admin/media/${id}`, "delete"),
  moveFile: (params: any) => API.request(`/admin/file/move`, "post", params),
  copyFile: (params: any) => API.request(`/admin/file/copy`, "post", params),
  createFolder: (path: string) =>
    API.request(`/admin/file/createFolder`, "post", { path }),
  deleteFolder: (path: string) =>
    API.request(`/admin/file/deleteFolder`, "post", { path })
};
