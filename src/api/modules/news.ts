import API from "@/api";
import { makeQueryURL } from "@/helpers/utils/functions";
import { DataInterface, NewsInterface } from "@/helpers/types";
export default {
  getNews: (params: any) =>
    API.request(`/api/news?${makeQueryURL(params)}`, "get"),
  getNew: (id: number) => API.request(`/admin/news/${id}`, "get"),
  makeReadNews: (id: number) => API.request(`/api/news/${id}`, "put"),
  sendNews: (params: any) => API.request(`/admin/email/send`, "post", params),
  getRatingWithFilter: (month: any, year: any) =>
    API.request(`/admin/rating?month=${month}&year=${year}`, "get")
};
