import API from "@/api";
import { DataInterface, UserInterface } from "@/helpers/types";

export default {
  deleteUsers: (params: any) =>
    API.request(`/admin/delete/user`, "delete", params),
  blockUsers: (params: any) => API.request(`/admin/block/user`, "put", params),
  unblockUsers: (params: any) =>
    API.request(`/admin/active/user`, "put", params),
  checkPayments: () =>
    API.request(`/admin/payment/checkPayment`, "get"),
};
