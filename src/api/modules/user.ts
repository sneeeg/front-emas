import API from "@/api";
import { DataInterface, UserInterface } from "@/helpers/types";

export default {
  getProfile: () => API.request(`/api/user`, "get"),
  addUsers: (params: { emails: string[]; message: string }) =>
    API.request(`/admin/registers`, "post", params),
  getUser: (id: number) => API.request(`/admin/user/${id}`, "get"),
  getUsers: (params: DataInterface<UserInterface[]>) =>
    API.request(`/admin/user?${params}`, "get"),
  getUsersSlug: (params: DataInterface<UserInterface[]>) =>
    API.request(`/admin/user?${params}`, "get"),
  editUser: (id: number, params: UserInterface) =>
    API.request(`/admin/user/${id}`, "put", params),
  editUserActivity: (id: number, social: any, params: any) =>
    API.request(`/admin/user/${id}/social/${social}`, "put", params),
  getUserPlan: () => API.request(`/api/user/getPlan`, "get"),
  changePass: (params: any) => API.request(`/api/changePassword`, "put", params),
  getRating: () => API.request(`/api/user/getRating`, "get"),
  sendWelcomeEmail: (id: number) => API.request(`/admin/user/${id}/email/sendPassword`, "get"),
  getUserPayment: () => API.request(`/api/dataPayment`, "get"),
  resetPayment: (id: number) => API.request(`/admin/payment/resetPayment/${id}`, "put"),
  getAdminUserPlan: (id: UserInterface["id"]) =>
    API.request(`/admin/user/${id}/getPlan`, "get"),
  getVideo: (id: number) =>
    API.request(`/api/streamVideo?media_id=${id}`, "get"),
  getEmasPrograms: (lang: string) =>
    API.request(`/api/price`, "get"),
  deleteUser: (id: number) => API.request(`/admin/user/${id}`, "delete"),
  getSocialActivity: (id: UserInterface["id"]) =>
    API.request(`/admin/user/${id}/social`, "get"),
  setUserAvatar: (id: number, params: { file: File }) =>
    API.request(`/admin/user/${id}/setAvatar`, "post", params),
  setScore: (id: number, params: any) =>
    API.request(`/admin/user/${id}/setScore`, "post", params)
};
