import API from "@/api";
import { makeQueryURL } from "@/helpers/utils/functions";

export default {
  getAllNetworkingFields: () => API.request("/api/networking", "get"),
  saveAllNetworkingFileds: (data: any) =>
    API.request("/api/networking", "put", data),

  toSearchNetwororking: (data: any) =>
    API.request(`/api/search/networking?${makeQueryURL(data)}`, "get"),

  saveNetworkingStatus: (status: any) => API.request("/api/user", "put", status)
};
