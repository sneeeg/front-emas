import API from "@/api";

export default {
  getRating: () => API.request(`/admin/rating`, "get"),
  getRatingWithFilter: (month: any, year: any) =>
    API.request(`/admin/rating?month=${month}&year=${year}`, "get")
};
