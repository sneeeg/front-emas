import API from "@/api";

export default {
  getSettings: () => API.request(`/admin/setting`, "get"),
  getSettingSocial: (id: number) => API.request(`/api/social/link`, "get"),
  getSetting: (id: number) => API.request(`/admin/setting/${id}`, "get"),
  editSetting: (params: any) =>
    API.request(`/admin/setting/${params.id}`, "put", params)
};
