import API from "@/api";
import { DataInterface } from "@/helpers/types";

export default {
  getPrices: () => API.request(`/admin/price`, "get"),
  getPriceById: (id: number) => API.request(`/admin/price/${id}`, "get"),
  addPrice: (params: any) => API.request(`/admin/price`, "post", params),
  updatePrice: (params: any) => API.request(`/admin/price/${params.id}`, "put", params),
  deletePriceById: (id: number) => API.request(`/admin/price/${id}`, "delete")
};
