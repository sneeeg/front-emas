import API from "@/api";
import {
  Maybe,
  ReportTypes,
  ReportResponseType,
  ReportRequestParamsType,
  ReportType
} from "@/helpers/types";
import { REPORT_URL } from "@/helpers/constants";
import { makeQueryURL } from "@/helpers/utils/functions";

export default {
  getReport: (
    type: ReportTypes,
    params: ReportRequestParamsType
  ): Maybe<ReportResponseType<ReportType[]>> => {
    let url: string;
    const {
      quiz_id,
      user_id,
      users,
      program_id,
      group_id,
      date_start,
      date_end,
      hide_unassigned,
      step
    } = params;

    switch (type) {
      case ReportTypes.QUIZ_SCORE_RESULTS:
        url = REPORT_URL.QUIZ_SCORE_RESULTS;
        params = { quiz_id, group_id, date_start, date_end };
        break;
      case ReportTypes.QUIZ_ANSWER_BREAKDOWN:
        url = REPORT_URL.QUIZ_ANSWER_BREAKDOWN;
        params = { quiz_id, group_id, date_start, date_end };
        break;
      case ReportTypes.LEARNING_PATH_SUMMARY:
        url = REPORT_URL.LEARNING_PATH_SUMMARY;
        params = { program_id, group_id, hide_unassigned };
        break;
      case ReportTypes.LOOKED_NEWS:
        url = REPORT_URL.LOOKED_NEWS;
        params = { users };
        break;
      case ReportTypes.SUBMITTED_PROJECT:
        url = REPORT_URL.SUBMITTED_PROJECT;
        params = { users };
        break;
      case ReportTypes.USER_ACTIVITY:
        url = REPORT_URL.USER_ACTIVITY;
        params = { user_id, date_start, date_end };
        break;
      case ReportTypes.TRAFFIC_LIGHTS:
        url = REPORT_URL.TRAFFIC_LIGHTS;
        params = { date_start, date_end, step };
        break;
      default:
        return null;
    }
    return API.request(`${url}?${makeQueryURL(params)}`, "get");
  },
  exportReport: (params: any) =>
    API.request(`/admin/excel/export?${makeQueryURL(params)}`, "get"),
  setCommentToDebt: (id: number, params: any) =>
    API.request(`/admin/debt/${id}`, "put", params)
};
