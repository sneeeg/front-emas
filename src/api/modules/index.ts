import auth from "./auth";
import user from "./user";
import users from "./users";
import course from "./course";
import courses from "./courses";
import files from "./files";
import group from "./group";
import rating from "./rating";
import report from "./report";
import profile from "./profile";
import quiz from "./quiz";
import news from "./news";
import media from "./media";
import networking from "./networking";
import price from "./price";
import project from "./project";
import settings from "./settings";

const modules = {
  auth,
  user,
  users,
  course,
  courses,
  files,
  group,
  rating,
  profile,
  report,
  quiz,
  news,
  media,
  networking,
  price,
  project,
  settings
};

export { modules };
