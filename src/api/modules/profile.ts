import API from "@/api";
import { DataInterface, UserInterface } from "@/helpers/types";

export default {
  editProfile: (params: UserInterface) =>
    API.request(`/api/user`, "put", params),
  setProfileAvatar: (params: { file: File }) =>
    API.request(`/api/user/setAvatar`, "post", params)
};
