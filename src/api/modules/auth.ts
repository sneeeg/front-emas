import API from "@/api";
import { client_id, client_secret, grant_type } from "@/config";

export default {
  register: (params: any) => API.request(`/api/register`, "post", params),
  login: (params: any) => {
    return API.request(`/api/token`, "post", {
      ...params,
      client_secret,
      client_id,
      grant_type
    });
  }
};
