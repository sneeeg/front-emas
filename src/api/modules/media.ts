import API from "@/api";

export default {
  SubmitProject: (id: number, params: { file: File }) =>
    API.request(`/api/media/${id}/submittedProject`, "post", params),
  ScoreAndTrainerForSubmitProject: (user_id: number, media_id: number, submit_id: number, params: { trainer_id: number, score: number }) =>
    API.request(`admin/user/${user_id}/media/${media_id}/submittedProject/${submit_id}`, "put", params),
  DownloadSubmitProject: (media_id: number, submit_id: number) =>
    API.request(`/api/media/${media_id}/submittedProject/${submit_id}`, "get"),

  SendFilesFromSubmittedProject: (user_id: number, media_id: number, submit_id: number, params: any) =>
    API.request(`admin/user/${user_id}/media/${media_id}/submittedProject/${submit_id}`, "put", params),
};
