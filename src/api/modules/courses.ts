import API from "@/api";
import { CourseInterface, DataInterface } from "@/helpers/types";

export default {
  getCourses: (params: DataInterface<CourseInterface[]>) =>
    API.request(`/admin/program?${params}`, "get"),
  getUserCourses: (params: string) =>
    API.request(`/api/program?${params}`, "get"),
  duplicateCourses: (params: any) =>
    API.request(`/admin/duplicate/program`, "post", params),
  deleteCourses: (params: any) =>
    API.request(`/admin/delete/program`, "delete", params)
};
