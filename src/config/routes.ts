import { URL } from "@/helpers/constants";
import { RouteType } from "@/router/utils";
import { UserRole } from "@/helpers/types";
import "@/i18n";
const list: RouteType[] = [
  {
    name: "profile.index",
    path: `${URL.PROFILE}`,
    component: "profile/index",
    menuIcon: "user-circle",
    meta: {
      title: "personal_data",
      roles: [UserRole.student, UserRole.admin, UserRole.superadmin],
      layout: "default"
    },
    private: true,
    disabled: true
  },
  {
    name: "index",
    path: URL.INDEX,
    component: "courses/index",
    meta: {
      title: "main",
      roles: [UserRole.admin, UserRole.superadmin, UserRole.student],
      layout: "default"
    },
    private: true,
    redirect: `${URL.COURSES}`,
    disabled: false
  },
  {
    name: "courses.admin",
    path: URL.COURSES,
    component: "courses/index",
    menuIcon: "clipboard-list",
    roleComponent: {
      [UserRole.student]: "courses/student"
    },
    meta: {
      title: "programs",
      roleTitle: {
        [UserRole.student]: "my_courses"
      },
      roles: [UserRole.admin, UserRole.superadmin],
      layout: "admin"
    },
    private: true,
    disabled: false
  },
  {
    name: "courses.index",
    path: `${URL.COURSES}/list`,
    menuIcon: "clipboard-list",
    component: "courses/student",
    meta: {
      title: "programs",
      roleTitle: {
        [UserRole.student]: "my_courses"
      },
      roles: [UserRole.student, UserRole.admin, UserRole.superadmin],
      layout: "default"
    },
    private: true,
    disabled: true
  },
  {
    name: "score.index",
    path: `${URL.SCORE}`,
    component: "score/index",
    menuIcon: "user-circle",
    meta: {
      title: "Your_grades",
      roles: [UserRole.student, UserRole.admin, UserRole.superadmin],
      layout: "default"
    },
    private: true,
    disabled: true
  },
  {
    name: 'networking.index',
    path: `${URL.NETWORKING}`,
    component: "networking/index",
    menuIcon: "user-circle",
    meta: {
      title: "network",
      roles: [UserRole.student, UserRole.admin, UserRole.superadmin],
      layout: "default"
    },
    private: true,
    disabled: true
  },
  {
    name: "payment.index",
    path: `${URL.PAYMENT}`,
    component: "payment/index",
    menuIcon: "money-bill-alt",
    meta: {
      title: "Payment_for_education",
      roles: [UserRole.student, UserRole.admin, UserRole.superadmin],
      layout: "default"
    },
    private: true,
    disabled: true
  },
  //{
  //  name: "courses.done",
  //  path: URL.COURSES_DONE,
  //  menuIcon: "clipboard-list",
  //  component: "courses/done",
  //  meta: {
  //    title: "Завершенные курсы",
  //    roles: [UserRole.student],
  //    layout: "default"
  //  },
  //  private: true
  //},
  {
    name: "courses.show",
    path: `${URL.COURSES}/:id`,
    component: "courses/show",
    meta: {
      title: "program",
      roles: [UserRole.admin, UserRole.superadmin, UserRole.superadmin, UserRole.student],
      navBack: `${URL.COURSES}`,
      layout: "default"
    },
    private: true,
    disabled: false
  },
  {
    name: "courses.edit",
    path: `${URL.COURSES}/:id/edit`,
    component: "courses/edit",
    meta: {
      title: "manage_program",
      roles: [UserRole.admin, UserRole.superadmin],
      navBack: `${URL.COURSES}`,
      layout: "admin"
    },
    private: true,
    disabled: false
  },
  {
    name: "users.index",
    path: URL.USERS,
    menuIcon: "user",
    component: "users/index",
    meta: {
      title: "users.title",
      roles: [UserRole.admin, UserRole.superadmin, UserRole.moderator],
      layout: "admin"
    },
    private: true,
    disabled: false
  },
  {
    name: "project.index",
    path: URL.PROJECTS,
    menuIcon: "project-diagram",
    component: "project/index",
    meta: {
      title: "project_title",
      roles: [UserRole.admin, UserRole.superadmin, UserRole.trainer],
      layout: "admin"
    },
    private: true,
    disabled: false
  },
  {
    name: "project.edit",
    path: `${URL.PROJECTS}/:id/edit`,
    menuIcon: "project-diagram",
    component: "project/edit",
    meta: {
      title: "project_title",
      roles: [UserRole.admin, UserRole.superadmin, UserRole.trainer],
      layout: "admin",
      navBack: `${URL.PROJECTS}`
    },
    private: true,
    disabled: true
  },
  
  {
    name: "news.index",
    path: URL.NEWS,
    menuIcon: "newspaper",
    component: "news/index",
    meta: {
      title: "Messages",
      roles: [UserRole.student, UserRole.admin, UserRole.superadmin],
      layout: "default"
    },
    private: true,
    disabled: true
  },
  {
    name: "news.show",
    path: `${URL.NEWS}/:id`,
    component: "news/show",
    meta: {
      title: "Message",
      roles: [UserRole.admin, UserRole.superadmin, UserRole.student],
      navBack: `${URL.NEWS}`,
      layout: "default"
    },
    private: true,
    disabled: false
  },
  {
    name: "users.edit",
    path: `${URL.USERS}/:id/edit`,
    component: "users/edit",
    meta: {
      title: "users.edit",
      roles: [UserRole.admin, UserRole.superadmin, UserRole.moderator],
      navBack: `${URL.USERS}`,
      layout: "admin"
    },
    private: true,
    disabled: false
  },
  //{
  //  name: "profile.index",
  //  path: `${URL.PROFILE}`,
  //  component: "profile/index",
  //  meta: {
  //    title: "Мой профиль",
  //    roles: [UserRole.admin, UserRole.superadmin, UserRole.student],
  //    layout: "default"
  //  },
  //  private: true
  //},
  {
    name: "file-manager.index",
    path: URL.FILE_MANAGER,
    menuIcon: "file",
    component: "file-manager/index",
    meta: {
      title: "file_manager.title",
      roles: [UserRole.admin, UserRole.superadmin, UserRole.trainer],
      layout: "admin"
    },
    private: true,
    disabled: false
  },
  {
    name: "file-manager.path",
    path: `${URL.FILE_MANAGER}/:path*`,
    component: "file-manager/index",
    meta: {
      title: "file_manager.title",
      roles: [UserRole.admin, UserRole.superadmin, UserRole.trainer],
      navBack: `${URL.FILE_MANAGER}`,
      layout: "admin"
    },
    private: true,
    disabled: false
  },
  //{
  //  name: "content.info.path",
  //  path: `${URL.CONTENT}/info/:id`,
  //  component: "content/info",
  //  meta: {
  //    title: "file_manager.title",
  //    roles: [UserRole.admin, UserRole.superadmin],
  //    navBack: true,
  //    layout: "admin"
  //  },
  //  private: true,
  //  disabled: false
  //},
  {
    name: "content.info.path",
    path: `${URL.CONTENT}/info/:id`,
    component: "content/info",
    meta: {
      title: "video",
      roles: [UserRole.student, UserRole.admin, UserRole.superadmin],
      navBack: `${URL.CONTENT}`,
      layout: "default"
    },
    private: true,
    disabled: false
  },
  {
    name: "rating.index",
    path: `${URL.RATING}`,
    component: "rating/index",
    menuIcon: "star",
    meta: {
      title: "rating.title",
      roles: [UserRole.admin, UserRole.superadmin],
      layout: "admin"
    },
    private: true,
    disabled: false
  },
  {
    name: "price.index",
    path: `${URL.PRICE}`,
    component: "price/index",
    menuIcon: "file-invoice-dollar",
    meta: {
      title: "price",
      roles: [UserRole.admin, UserRole.superadmin],
      layout: "admin"
    },
    private: true
  },
  {
    name: "price.edit",
    path: `${URL.PRICE}/:id/edit`,
    component: "price/edit",
    menuIcon: "file-invoice-dollar",
    meta: {
      title: "price",
      roles: [UserRole.admin, UserRole.superadmin],
      layout: "admin",
      navBack: `${URL.PRICE}`
    },
    private: true,
    disabled: true
  },
  {
    name: "documents.admin",
    path: `${URL.DOCUMENTS}/admin`,
    component: "documents/admin",
    menuIcon: "print",
    meta: {
      title: "documents",
      roles: [UserRole.admin, UserRole.superadmin],
      layout: "admin"
    },
    private: true
  },
  {
    name: "documents.edit",
    path: `${URL.DOCUMENTS}/:id/edit`,
    component: "documents/edit",
    meta: {
      title: "material-edit",
      roles: [UserRole.admin, UserRole.superadmin],
      navBack: `${URL.DOCUMENTS}/admin`,
      layout: "admin"
    },
    private: true,
    disabled: false
  },
  {
    name: "documents.index",
    path: `${URL.DOCUMENTS}`,
    component: "documents/index",
    menuIcon: "print",
    meta: {
      title: "Download_the_documents",
      roles: [UserRole.student, UserRole.admin, UserRole.superadmin],
      layout: "default"
    },
    private: true,
    disabled: true
  },
  {
    name: "documents.upload",
    path: `${URL.UPLOAD}`,
    component: "documents/upload",
    menuIcon: "upload",
    meta: {
      title: "Upload_the_documents",
      roles: [UserRole.student, UserRole.admin, UserRole.superadmin],
      layout: "default"
    },
    private: true,
    disabled: true
  },
  {
    name: "reports.index",
    path: `${URL.REPORTS}`,
    component: "reports/index",
    menuIcon: "chart-bar",
    meta: {
      title: "reports",
      roles: [UserRole.admin, UserRole.superadmin, UserRole.moderator],
      layout: "admin"
    },
    private: true,
    disabled: false
  },
  {
    name: "report.index",
    path: `${URL.REPORTS}/:id`,
    component: "report/index",
    meta: {
      title: "report",
      roles: [UserRole.admin, UserRole.superadmin, UserRole.moderator],
      navBack: `${URL.REPORTS}`,
      layout: "admin"
    },
    private: true,
    disabled: false
  },
  {
    name: "report.view",
    path: `${URL.REPORTS}/:id/view`,
    component: "report/view",
    meta: {
      title: "report-view",
      roles: [UserRole.admin, UserRole.superadmin, UserRole.moderator],
      navBack: `${URL.REPORTS}`,
      layout: "admin"
    },
    private: true,
    disabled: false
  },
  {
    name: "quizzes.admin.index",
    path: `${URL.QUIZZES}`,
    component: "quizzes/admin/index",
    menuIcon: "chart-bar",
    meta: {
      title: "quizzes",
      roles: [UserRole.admin, UserRole.superadmin],
      navBack: `${URL.QUIZZES}`,
      layout: "admin"
    },
    private: true,
    disabled: false
  },
  {
    name: "video.index",
    path: `${URL.VIDEOS}`,
    component: "video/index",
    menuIcon: "play",
    meta: {
      title: "video",
      roles: [UserRole.admin, UserRole.superadmin],
      navBack: `${URL.VIDEOS}`,
      layout: "admin"
    },
    private: true,
    disabled: true
  },
  {
    name: "main.send",
    path: `${URL.MAIL}`,
    component: "mail/send",
    menuIcon: "envelope",
    meta: {
      title: "send-message",
      roles: [UserRole.admin, UserRole.superadmin],
      navBack: `${URL.MAIL}`,
      layout: "admin"
    },
    private: true,
    disabled: false
  },
  {
    name: "file.edit",
    path: `${URL.FILE}/:id/edit`,
    component: "file/edit",
    meta: {
      title: "material-edit",
      roles: [UserRole.admin, UserRole.superadmin, UserRole.trainer],
      navBack: `${URL.FILE_MANAGER}`,
      layout: "admin"
    },
    private: true,
    disabled: false
  },
  {
    name: "quiz.admin.index",
    path: `${URL.QUIZZES}/:id/edit`,
    component: "quiz/admin/index",
    meta: {
      title: "quiz-view",
      roles: [UserRole.admin, UserRole.superadmin],
      layout: "admin"
    },
    private: true,
    disabled: false
  },
  {
    name: "quiz.admin.questions.edit",
    path: `${URL.QUIZZES}/:id/edit/question/:optionId`,
    component: "quiz/admin/questions/edit",
    meta: {
      title: "quiz-view",
      navBack: `${URL.QUIZZES}`,
      roles: [UserRole.admin, UserRole.superadmin],
      layout: "admin"
    },
    private: true,
    disabled: false
  },
  {
    name: "quiz.admin.show",
    path: `${URL.QUIZZES}/:id`,
    component: "quiz/admin/show",
    meta: {
      title: "quiz-pass",
      roles: [UserRole.admin, UserRole.superadmin],
      layout: "default"
    },
    private: true,
    disabled: false
  },
  // {
  //     name: "quizzes.client.index",
  //     path: `/client/quizzes`,
  //     component: "quizzes/client/index",
  //     menuIcon: "chart-bar",
  //     meta: {
  //         title: "Тесты",
  //         roles: [UserRole.admin, UserRole.superadmin],
  //         navBack: true,
  //         layout: "default"
  //     },
  //     private: true
  // },
  {
    name: "quiz.client.index",
    // FIXME: Исправить path на правильный
    path: `/client${URL.QUIZZES}/:id`,
    component: "quiz/client/index",
    meta: {
      title: "quiz-pass",
      roles: [UserRole.student],
      layout: "default"
    },
    private: true,
    disabled: false
  },

  {
    name: "auth.index",
    path: `${URL.AUTH}`,
    component: "auth/index",
    meta: {
      title: "Log_in_system",
      roles: [UserRole.admin, UserRole.superadmin, UserRole.student],
      navBack: `${URL.AUTH}`,
      layout: "auth"
    },
    disabled: false
  },
  {
    name: "404",
    path: `${URL.PAGE_NOT_FOUND}`,
    component: "404/index",
    meta: {
      title: "Страница не найдена",
      roles: [UserRole.admin, UserRole.superadmin, UserRole.student],
      //navBack: true,
      layout: "default"
    },
    private: true,
    disabled: false
  },
  {
    name: "setting.index",
    path: URL.SETTING,
    menuIcon: "cogs",
    component: "setting/index",
    meta: {
      title: "setting_title",
      roles: [UserRole.admin, UserRole.superadmin],
      layout: "admin"
    },
    private: true,
    disabled: false
  },
  {
    name: "setting.edit",
    path: `${URL.SETTING}/:id/edit`,
    menuIcon: "cogs",
    component: "setting/edit",
    meta: {
      title: "setting_title",
      roles: [UserRole.admin, UserRole.superadmin],
      layout: "admin",
      navBack: `${URL.SETTING}`
    },
    private: true,
    disabled: true
  }
];

export { list };
