const apiUrl = process.env.VUE_APP_API_URL;
const client_secret = process.env.VUE_APP_CLIENT_SECRET;
const client_id = process.env.VUE_APP_CLIENT_ID;
const grant_type = process.env.VUE_APP_GRANT_TYPE;
const document_title = process.env.VUE_APP_DOCUMENT_TITLE;

export { apiUrl, client_secret, client_id, grant_type, document_title };
